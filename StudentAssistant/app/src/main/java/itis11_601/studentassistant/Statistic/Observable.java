package itis11_601.studentassistant.Statistic;

/**
 * Created by 1 on 30.03.2017.
 */

public interface Observable {

    void register(OnStatisticListener o);
    void remove(OnStatisticListener o);
    void notifyListeners();
}
