package itis11_601.studentassistant.Statistic;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by lucky.mz on 13.04.2017.
 */

public class ParseDate {

    public static String parseData() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String dat = sdf.format(new Date(System.currentTimeMillis()));
        System.out.println(dat);
        return dat;
    }
}
