package itis11_601.studentassistant.Statistic.SleepStats;

import android.content.Context;
import android.icu.text.DateFormat;
import android.widget.ImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.R;
import itis11_601.studentassistant.UpcomingActivity;

/**
 * Created by avispa on 5/4/2017.
 */

public class SleepStatsHelper {
    private DBHelper dbHelper;
    private Context context;
    private static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    public SleepStatsHelper(Context context) {
        this.context = context;
        dbHelper = DBHelper.getDbHelper(context);
//        pickTime();
    }

//    public void pickTime() {
//        Calendar calendar = GregorianCalendar.getInstance();
//        int hour = calendar.get(GregorianCalendar.HOUR_OF_DAY);
//        int min = calendar.get(GregorianCalendar.MINUTE);
//        if (min > 45) ++hour;
//        if (hour > 23) Toast.makeText(context, "Time to sleep, bro", Toast.LENGTH_LONG).show();
//    }

    public static String formatDateTime(Context context, String timeToFormat) {
        String finalDateTime = "";

        SimpleDateFormat iso8601Format = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        Date date = null;
        if (timeToFormat != null) {
            try {
                date = iso8601Format.parse(timeToFormat);
            } catch (ParseException e) {
                date = null;
            }

            if (date != null) {
                long when = date.getTime();
                int flags = 0;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_TIME;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_DATE;
                flags |= android.text.format.DateUtils.FORMAT_ABBREV_MONTH;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_YEAR;

                finalDateTime = android.text.format.DateUtils.formatDateTime(context,
                        when + TimeZone.getDefault().getOffset(when), flags);
            }
        }
        return finalDateTime;
    }

    public static String formatDate(Date date) {
        return format.format(date);
    }

    public static Date parseDate(String date) {
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public double getAverageHoursLastWeek() {
        return dbHelper.getAverageHoursLastWeek();
    }

    public void addValue(int hours) {
        dbHelper.addHours(hours);
    }

    public void deleteLastHours() {
        dbHelper.deleteLastHours(context);
    }

    public int getLastDayHours() {
        return dbHelper.getLastDayHours();
    }

    public int getMinHoursLastWeek() {
        return dbHelper.getMinHoursLastWeek();
    }

    public ArrayList<String> getAllHoursLastWeek() {
        return dbHelper.getAllHoursLastWeek();
    }

    public int getMaxHoursLastWeek() {
        return dbHelper.getMaxHoursLastWeek();
    }

    public int getMinHoursLastMonth() {
        return dbHelper.getMinHoursLastMonth();
    }

    public int getMaxHoursLastMonth() {
        return dbHelper.getMaxHoursLastMonth();
    }

    public double getAverageHoursLastMonth() {
        return dbHelper.getAverageHoursLastMonth();
    }

    public static boolean compareDates(Date lastRecordDate, Date today) {
        try {
            return formatDate(lastRecordDate).equals(formatDate(today));
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean isPickedStatsToday() {
        return compareDates(dbHelper.getLastRecordDate(), new Date());
    }
}
