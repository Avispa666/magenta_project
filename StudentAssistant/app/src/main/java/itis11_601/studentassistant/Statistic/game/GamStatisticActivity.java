package itis11_601.studentassistant.Statistic.game;

import android.os.Bundle;
import android.widget.TextView;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;
import itis11_601.studentassistant.Assistant.AssistantLogic;

/**
 * Created by lucky.mz on 11.04.2017.
 */

public class GamStatisticActivity extends DrawerActivity {
    private TextView best;
    private TextView middle;
    private TextView bad;
    private TextView qt;
    private MultiStateToggleButton mstb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_statistic);
        setTitle("Статистика по тестам");
    }
    @Override
    protected void onFragmentCreated() {
        best = (TextView) findViewById(R.id.bestRes);
        middle = (TextView) findViewById(R.id.medRes);
        bad = (TextView) findViewById(R.id.badRes);
        qt = (TextView) findViewById(R.id.qtGames);
        mstb = (MultiStateToggleButton) this.findViewById(R.id.mstb_multi_id_game);
        mstb.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int position) {
                switch (position) {
                    case 0:
                        day();
                        break;
                    case 1:
                        week();
                        break;
                    case 2:
                        //month();
                }
            }
        });
        mstb.setValue(0);

//        week();
        //week.setTextColor(Color.BLUE);
    }

  /**  private void month() {
        GameStatReturnElem res = AssistantLogic.gameStatistic.searchingMonthResults();
        best.setText(res.getBest() + "");
        middle.setText(res.getMiddle() + "");
        bad.setText(res.getBad() + "");
        qt.setText(res.getQuantity() + "");
    }
*/
    private void week() {
        GameStatReturnElem res = AssistantLogic.gameStatistic.searchingWeekResults();
        best.setText(res.getBest() + "");
        middle.setText(res.getMiddle() + "");
        bad.setText(res.getBad() + "");
        qt.setText(res.getQuantity() + "");
    }

    private void day() {
        GameStatReturnElem res = AssistantLogic.gameStatistic.searchingDayResults();
        best.setText(res.getBest() + "");
        middle.setText(res.getMiddle() + "");
        bad.setText(res.getBad() + "");
        qt.setText(res.getQuantity() + "");
    }


}
