package itis11_601.studentassistant.Statistic.game;

import java.util.ArrayList;
import java.util.Calendar;

import itis11_601.studentassistant.Statistic.ParseDate;

/**
 * Created by lucky.mz on 30.03.2017.
 */
public class GameStatistic extends ParseDate {
    ArrayList<GameResult> results;


    /**    тестовый вариант
     public GameStatistic() {
     Calendar day = Calendar.getInstance();
     results = new ArrayList<>();
     results.add(new GameResult(9,day));
     results.add(new GameResult(57,day));
     Calendar week = Calendar.getInstance();
     week.add(Calendar.DATE,-6);
     results.add(new GameResult(88,week));
     results.add(new GameResult(12,week));
     Calendar calendar = Calendar.getInstance();
     calendar.add(Calendar.DATE,-16);
     results.add(new GameResult(100,calendar));
     results.add(new GameResult(10,calendar));

     }
  */
    public GameStatistic() {
        results = new ArrayList<>();
    }

    public void addResult(int result, Calendar date) {
        GameResult gameResult = new GameResult(result,date);
        results.add(gameResult);
        Calendar from = Calendar.getInstance();
        from.add(Calendar.DATE, -7);
        Calendar today = Calendar.getInstance();
    }

    public GameStatReturnElem searchingDayResults() {
        Calendar currentData = Calendar.getInstance();
        int max = 0;
        int min = 100;
        int sum = 0;
        int qt = 0;
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).getData().get(Calendar.DATE) == (currentData.get(Calendar.DATE))) {
                qt++;
                System.out.println(qt);
                sum += results.get(i).getResult();
                if (max < results.get(i).getResult()){
                    max = results.get(i).getResult();
                }
                if (min > results.get(i).getResult()) {
                    min = results.get(i).getResult();
                }
            }
        }
        if (qt == 0) {
            return new GameStatReturnElem(0, 0, 0, 0);
        }else
            return new GameStatReturnElem(max, sum / qt, min, qt);
    }

    public GameStatReturnElem searchingMonthResults() {
        int max = 0;
        int min = 100;
        int sum = 0;
        int qt = results.size();
        for (int i = 0; i < results.size(); i++) {
            sum += results.get(i).getResult();
            if (max < results.get(i).getResult()){
                max = results.get(i).getResult();
            }
            if (min > results.get(i).getResult()) {
                min = results.get(i).getResult();
            }
        }
        if (qt == 0) {
            return new GameStatReturnElem(0, 0, 0, 0);
        }else
            return new GameStatReturnElem(max, sum / qt, min, qt);
    }

    public GameStatReturnElem searchingWeekResults() {
        Calendar currentData = Calendar.getInstance();
        currentData.add(Calendar.DATE,-7);
        int max = 0;
        int min = 100;
        int sum = 0;
        int qt = 0;
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).getData().after(currentData)) {
                qt++;
                System.out.println(qt);
                sum += results.get(i).getResult();
                if (max < results.get(i).getResult()){
                    max = results.get(i).getResult();
                }
                if (min > results.get(i).getResult()) {
                    min = results.get(i).getResult();
                }
            }
        }
        if (qt == 0) {
            return new GameStatReturnElem(0, 0, 0, 0);
        }else
            return new GameStatReturnElem(max, sum / qt, min, qt);
    }

    public String getLastGameResult() {
        if (results != null && !results.isEmpty())
            return results.get(results.size() - 1).getResult() + "/100";
        else
        return "-";
    }
}



