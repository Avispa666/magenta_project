package itis11_601.studentassistant.Statistic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import itis11_601.studentassistant.Assistant.AssistantLogic;
import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;
import itis11_601.studentassistant.Statistic.SleepStats.SleepStatsActivity;
import itis11_601.studentassistant.Statistic.SleepStats.SleepStatsHelper;
import itis11_601.studentassistant.Statistic.game.GamStatisticActivity;

public class StatisticActivity extends DrawerActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
    }
        @Override
        protected void onFragmentCreated() {
            Button testStatBtn8 = (Button) findViewById(R.id.button8);
            testStatBtn8.setOnClickListener(this);
            Button testStatBtn7 = (Button) findViewById(R.id.button7);
            testStatBtn7.setOnClickListener(this);
            Button testStatBtn6 = (Button) findViewById(R.id.button6);
            testStatBtn6.setOnClickListener(this);
            Button testStatBtn5 = (Button) findViewById(R.id.button5);
            testStatBtn5.setText(String.valueOf(new SleepStatsHelper(this.getApplicationContext()).getLastDayHours()).concat("ч."));
            testStatBtn5.setOnClickListener(this);

            testStatBtn7.setText(AssistantLogic.gameStatistic.getLastGameResult());

        }

    @Override
    public void onClick(View view) {
        Intent gameTest = new Intent(this, GamStatisticActivity.class);
        Intent sleep = new Intent(this, SleepStatsActivity.class);
        switch (view.getId()){
            case R.id.button8:
                startActivity(gameTest);
                break;
            case R.id.button7:
                startActivity(gameTest);
                break;
            case R.id.button6:
                startActivity(sleep);
                break;
            case R.id.button5:
                startActivity(sleep);
                break;
        }
    }


}
