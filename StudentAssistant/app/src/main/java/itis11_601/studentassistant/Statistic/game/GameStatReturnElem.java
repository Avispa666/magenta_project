package itis11_601.studentassistant.Statistic.game;

/**
 * Created by lucky.mz on 13.04.2017.
 */

public class GameStatReturnElem {
    public GameStatReturnElem(int best, int middle, int bad, int quantity) {
        this.best = best;
        this.middle = middle;
        this.bad = bad;
        this.quantity = quantity;
    }

    private int best;
    private int middle;
    private int bad;
    private int quantity;

    public int getBest() {
        return best;
    }

    public void setBest(int best) {
        this.best = best;
    }

    public int getMiddle() {
        return middle;
    }

    public void setMiddle(int middle) {
        this.middle = middle;
    }

    public int getBad() {
        return bad;
    }

    public void setBad(int bad) {
        this.bad = bad;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
