package itis11_601.studentassistant.Statistic.SleepStats;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import java.text.DecimalFormat;

import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;
import itis11_601.studentassistant.Statistic.SleepStats.SleepStatsHelper;

public class SleepStatsActivity extends DrawerActivity implements View.OnClickListener {

    private SleepStatsHelper helper;
    private TextView averageTime;
    private TextView minHours;
    private TextView maxHours;
    private EditText timeField;
    private TextView sleepTimeToday;
    private TextView sleepTimeTodayField;
    private Button addBtn;
    private Button deleteLastBtn;
    private ListView hoursLv;
    private ArrayAdapter<String> adapter;
    private MultiStateToggleButton mstb;
    private Mode lastMode;
    private static final String TAG = "SleepStatsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep_stats);
        setTitle("Статистика сна");

    }

    @Override
    protected void onFragmentCreated() {
        helper = new SleepStatsHelper(this);
        averageTime = (TextView) findViewById(R.id.average_sleep_time);
        minHours = (TextView) findViewById(R.id.min_sleep_time);
        maxHours = (TextView) findViewById(R.id.max_sleep_time);
        timeField = (EditText) findViewById(R.id.insertTimeField);
        sleepTimeToday = (TextView) findViewById(R.id.sleepTimeToday);
        sleepTimeTodayField = (TextView) findViewById(R.id.sleepTimeTodayField);
        addBtn = (Button) findViewById(R.id.addBtn);
        deleteLastBtn = (Button) findViewById(R.id.deleteLastBtn);
        addBtn.setOnClickListener(this);
        deleteLastBtn.setOnClickListener(this);
        hoursLv = (ListView) findViewById(R.id.hoursLv);
//        hoursLv.setVisibility(View.GONE);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, helper.getAllHoursLastWeek());
        hoursLv.setAdapter(adapter);
        Log.d(TAG, "onFragmentCreated: " + helper.getAllHoursLastWeek());
        mstb = (MultiStateToggleButton) this.findViewById(R.id.mstb_multi_id);
        mstb.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int position) {
                switch (position) {
                    case 0:
                        update(Mode.DAY);
                        break;
                    case 1:
                        update(Mode.WEEK);
                        break;
                }
            }
        });
        mstb.setValue(0);
        update(Mode.DAY);

    }

    private enum Mode {
        DAY, WEEK
    }

    public void update(Mode mode) {
        lastMode = mode;
        double average = 0;
        int min = 0;
        int max = 0;
        switch (mode) {
            case DAY:
//                min = max = helper.getLastDayHours();
//                average = max;
                sleepTimeTodayField.setVisibility(View.VISIBLE);
                sleepTimeToday.setVisibility(View.VISIBLE);
                sleepTimeTodayField.setText(String.valueOf(helper.getLastDayHours()).concat("ч."));
                paintText(sleepTimeTodayField, helper.getLastDayHours());
                maxHours.setVisibility(View.GONE);
                findViewById(R.id.textView4).setVisibility(View.GONE);
                findViewById(R.id.textView7).setVisibility(View.GONE);
                findViewById(R.id.textView8).setVisibility(View.GONE);
                minHours.setVisibility(View.GONE);
                averageTime.setVisibility(View.GONE);
                hoursLv.setVisibility(View.GONE);
                break;
            case WEEK:
                min = helper.getMinHoursLastWeek();
                max = helper.getMaxHoursLastWeek();
                average = helper.getAverageHoursLastWeek();
                sleepTimeTodayField.setVisibility(View.GONE);
                sleepTimeToday.setVisibility(View.GONE);
                maxHours.setVisibility(View.VISIBLE);
                minHours.setVisibility(View.VISIBLE);
                averageTime.setVisibility(View.VISIBLE);
                findViewById(R.id.textView4).setVisibility(View.VISIBLE);
                findViewById(R.id.textView7).setVisibility(View.VISIBLE);
                findViewById(R.id.textView8).setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                hoursLv.setVisibility(View.VISIBLE);
                maxHours.setText(String.valueOf(max).concat("ч."));
                minHours.setText(String.valueOf(min).concat("ч."));
                paintText(maxHours, max);
                paintText(minHours, min);
                averageTime.setText(new DecimalFormat("#0.00").format(average).concat("ч."));
                paintText(averageTime, average);
                break;
        }

    }

    private void paintText(TextView tv, double hours) {
        if (hours > 7) tv.setTextColor(Color.GREEN);
        else if (hours < 5) tv.setTextColor(Color.RED);
        else tv.setTextColor(Color.rgb(204, 204, 0));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addBtn:
                if (timeField.getText().toString().equals("")) break;
                try {
                    int hour = Integer.parseInt(timeField.getText().toString());
                    if (hour <= 0 || hour > 24) throw new NumberFormatException();
                    helper.addValue(hour);
                } catch (NumberFormatException e) {
                    Toast.makeText(this, "Wrong data", Toast.LENGTH_LONG).show();
                } finally {
                    timeField.setText("");
                }
                update(lastMode);
                break;
            case R.id.deleteLastBtn:
                Log.d("activity", "onClick: delete");
                helper.deleteLastHours();
                update(lastMode);
        }
    }
}
