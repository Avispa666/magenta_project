package itis11_601.studentassistant.Statistic.game;


import java.util.Calendar;

import itis11_601.studentassistant.Statistic.OnStatisticListener;
import itis11_601.studentassistant.Statistic.ParseDate;
import itis11_601.studentassistant.Assistant.AssistantLogic;


/**
 * Created by lucky.mz on 30.03.2017.
 */

public class GameManager extends ParseDate implements OnStatisticListener {
    private int currentResult;
    private Calendar data;


    @Override
    public void update(int result) {
        data = Calendar.getInstance();
        currentResult = result;
        AssistantLogic.gameStatistic.addResult(currentResult,data);
    }



    //заглушка, парсится результат из игр в 100 бальной системе
    private int parseResult() {
        return 0;
    }



}
