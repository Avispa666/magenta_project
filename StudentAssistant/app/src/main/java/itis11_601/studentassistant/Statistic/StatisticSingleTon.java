package itis11_601.studentassistant.Statistic;

import itis11_601.studentassistant.Statistic.game.GameStatistic;

/**
 * Created by 1 on 30.03.2017.
 */
public class StatisticSingleTon {

    static GameStatistic statistic;

    public static GameStatistic getStatistic() {
        if (statistic == null) {
            statistic = new GameStatistic();
            return statistic;
        }
        return statistic;
    }
}
