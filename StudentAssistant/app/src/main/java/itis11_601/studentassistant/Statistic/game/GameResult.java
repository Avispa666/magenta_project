package itis11_601.studentassistant.Statistic.game;


import java.util.Calendar;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/**
 * Created by lucky.mz on 13.04.2017.
 */

public class GameResult {
    private int result;
    private Calendar data;

    public int getResult() {
        return result;
    }

    public GameResult(int result, Calendar data) {
        this.result = result;
        this.data = data;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public Calendar getData() {
        return data;
    }

    public void setData(Calendar data) {
        this.data = data;
    }


}
