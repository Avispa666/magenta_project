package itis11_601.studentassistant.GameTiles;

import android.util.Log;

import java.util.Random;

/**
 * Created by avispa on 26/3/2017.
 */

public class GameTiles {
    private static GameTiles game;
    private boolean field[][];
    private int size;
    private static final String TAG = "Game_logic";
    private static final int initialSize = 3;

    private GameTiles() {
        size = initialSize;
        generateField(size);
    }

    private void generateField(int size) {
        this.field = new boolean[size][size]; //0 is black, 1 is white
        Random random = new Random();
        for (int i = 0; i < size; ) {
            int x = Math.abs(random.nextInt()) % size;
            int y = Math.abs(random.nextInt()) % size;
            if (this.field[x][y]) continue;
            this.field[x][y] = true; //set white
            ++i;
        }
    }

    public boolean[][] getField() {
        Log.d(TAG, "getField: getting field");
        return this.field;
    }

    public boolean submitAnswer(boolean[][] answer) {
        boolean isRight = true;
        for (int x = 0; x < answer.length && isRight; x++) {
            for (int y = 0; y < answer[0].length && isRight; y++) {
                if (answer[x][y] != this.field[x][y]) {
                    isRight = false;
                    Log.d(TAG, "submitAnswer: incorrect");
                }
            }
        }
        if (isRight) inc();
        else dec();
        generateField(this.size);
        return isRight;
    }

    private void inc() {
        ++this.size;
    }

    private void dec() {
        if (this.size > initialSize) --this.size;
    }

    public static GameTiles getGame() {
        if (game == null) game = new GameTiles();
        return game;
    }

    public static GameTiles start() {
        game = new GameTiles();
        return game;
    }





}
