package itis11_601.studentassistant.UnnecessaryGame;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;

import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;

/**
 * Created by Максим on 26.03.2017.
 */
public class UnnecessaryActivity extends DrawerActivity implements OnGameFinishedListener {

    private Game game;
    private int level = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unnecessary_game_layout);
    }

    @Override
    protected void onFragmentCreated() {
        game = new Game(level++, this);
        game.addOnGameFinishedListener(this);
    }

    @Override
    public void gameFinished(boolean won) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        if (!won) level = 0;
        dialog.setTitle("Лишний")
                .setCancelable(false)
                .setMessage(won ? "Вы победили" : "Вы проиграли")
                .setPositiveButton(won ? "Далее" :"Еще раз", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onFragmentCreated();
                    }
                })
                .setNegativeButton("Выйти", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UnnecessaryActivity.super.onBackPressed();
                    }
                }).create();
        dialog.show();
    }
}
