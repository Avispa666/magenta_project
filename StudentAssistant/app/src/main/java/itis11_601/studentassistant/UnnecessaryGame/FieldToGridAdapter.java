package itis11_601.studentassistant.UnnecessaryGame;

import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.List;


/**
 * Created by Максим on 26.03.2017.
 */
public class FieldToGridAdapter extends BaseAdapter {

    private List<Field> fields;
    private Context context;
    private LayoutInflater inflater;

    public FieldToGridAdapter(List<Field> fields, Context context) {
        this.fields = fields;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return fields.size();
    }

    @Override
    public Field getItem(int i) {
        return fields.get(i);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup viewGroup) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(120, 120));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setPadding(16, 16, 16, 16);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageBitmap(fields.get(pos).getBitmap());
        return imageView;
    }
}
