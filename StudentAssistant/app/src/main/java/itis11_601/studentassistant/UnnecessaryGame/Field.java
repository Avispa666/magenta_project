package itis11_601.studentassistant.UnnecessaryGame;

import android.graphics.Bitmap;
import android.support.annotation.DrawableRes;

/**
 * Created by Максим on 26.03.2017.
 */
public class Field {
    private Bitmap bitmap;
    private boolean necessary;

    public Field(Bitmap bitmap, boolean necessary) {
        this.bitmap = bitmap;
        this.necessary = necessary;
    }


    public boolean isNecessary() {
        return necessary;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
