package itis11_601.studentassistant.UnnecessaryGame;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Максим on 26.03.2017.
 */
public class RandomSetOfResId {

    private Queue<Bitmap> que;

    public static final int AMOUNT_OF_ASSETS = 160;

    public RandomSetOfResId(Context context) {
        AssetManager assetManager = context.getAssets();
        LinkedList<Bitmap> a = new LinkedList<>();
        for (int i = 1; i <= AMOUNT_OF_ASSETS; i++) {
            a.add(getBitmapFromAsset(getFileName(i),assetManager));
        }
        ArrayHelper.shuffle(a);
        que = new LinkedList<>(a);

    }

    private String getFileName(int n) {
        String res = "unnecessary_game_assets/genericItem_color_";
        String png = ".png";
        String intPart = Integer.toString(n);
        while (intPart.length() < 3) {
            intPart = "0" + intPart;
        }
        return res + intPart + png;
    }

    private Bitmap getBitmapFromAsset(String strName, AssetManager assetManager) {
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
            Bitmap bitmap = BitmapFactory.decodeStream(istr);
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (istr != null) {
                try {
                    istr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        throw new RuntimeException("THERE IS NO PICTURE WITH NAME "  + strName);
    }

    public Bitmap getNext() {
        return que.poll();
    }
}
