package itis11_601.studentassistant.UnnecessaryGame;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import itis11_601.studentassistant.R;

/**
 * Created by Максим on 26.03.2017.
 */
public class Game implements AdapterView.OnItemClickListener {
    private List<OnGameFinishedListener> onGameFinishedListeners;
    private final int level;
    private Activity context;
    private int n;
    private List<Field> fields;
    private Field unnecessary;
    private GridView gView;
    public static final int LEVEL_N_OF_ITEMS[] = {
            3, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 9, 10,
    };
    private TextView score,timer;

    public Game(int level, Activity context) {
        onGameFinishedListeners = new ArrayList<>();
        this.level = level;
        score = (TextView) context.findViewById(R.id.score_unnecessary);
        timer = (TextView) context.findViewById(R.id.timer_unnecessary);
        score.setText("" + level);
        this.context = context;
        gView = (GridView) context.findViewById(R.id.greed_unnecessary);
        init();
    }


    private void init() {
        n = LEVEL_N_OF_ITEMS[Math.min(LEVEL_N_OF_ITEMS.length - 1, level)];
        RandomSetOfResId rnd = new RandomSetOfResId(context);
        fields = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            fields.add(new Field(rnd.getNext(), true));
        }
        unnecessary = new Field(rnd.getNext(), false);
        startCounting();
    }

    private void rebuildAdapter() {
        FieldToGridAdapter adapter = new FieldToGridAdapter(fields, context);
        gView.setAdapter(adapter);
    }


    private void startCounting() {
        rebuildAdapter();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int n = 6;
                    for (int i = 0; i < n; i++) {
                        final int time = n - i;
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                timer.setText("" + time);
                            }
                        });
                        Thread.sleep(1000);
                    }
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fields.add(unnecessary);
                            ArrayHelper.shuffle(fields);
                            gView.setOnItemClickListener(Game.this);
                            rebuildAdapter();

                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void addOnGameFinishedListener(OnGameFinishedListener listener) {
        onGameFinishedListeners.add(listener);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        boolean won = !fields.get(pos).isNecessary();
        for (OnGameFinishedListener listener : onGameFinishedListeners) {
            listener.gameFinished(won);
        }
    }


}
