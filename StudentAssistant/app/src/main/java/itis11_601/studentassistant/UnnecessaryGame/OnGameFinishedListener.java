package itis11_601.studentassistant.UnnecessaryGame;

/**
 * Created by Максим on 26.03.2017.
 */
public interface OnGameFinishedListener {
    void gameFinished(boolean won);
}
