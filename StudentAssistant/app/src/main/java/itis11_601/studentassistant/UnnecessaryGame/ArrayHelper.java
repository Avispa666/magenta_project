package itis11_601.studentassistant.UnnecessaryGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Максим on 26.03.2017.
 */
public class ArrayHelper {

    public static <E> void  shuffle(List<E> a) {
        Random rnd = new Random();
        for (int i = a.size() - 1; i >= 0; i--) {
            int ind = rnd.nextInt(i + 1);
            E tmp = a.get(i);
            a.set(i, a.get(ind));
            a.set(ind, tmp);
        }
    }
}
