package itis11_601.studentassistant;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Calendar;

import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.Subject.Subject;

public class Lesson extends Event implements Parcelable {

    private Subject subject;
    private int period;
    private Context context;

    public Lesson(long id, ArrayList<Note> notes, String name, String discript, Calendar dateStart, Calendar dateEnd, Subject subject, int period, int colorId) {
        super(id ,notes, name, discript, dateStart, dateEnd, colorId);
        this.subject = subject;
        this.period = period;
    }
    public Lesson(long id, String name, String discript, Calendar dateStart, Calendar dateEnd, Subject subject, int period, int colorId) {
        this(id, null, name, discript, dateStart, dateEnd, subject, period, colorId);
    }

    public Lesson(String name, String discript, Calendar dateStart, Calendar dateEnds, Subject subject, int period) {
        this(0, null, name, discript, dateStart, dateEnds, subject, period, android.R.color.holo_blue_dark);
    }

    protected Lesson(Parcel in) {
        super(in);
        subject = in.readParcelable(Subject.class.getClassLoader());
        period = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(subject, flags);
        dest.writeInt(period);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Lesson> CREATOR = new Creator<Lesson>() {
        @Override
        public Lesson createFromParcel(Parcel in) {
            return new Lesson(in);
        }

        @Override
        public Lesson[] newArray(int size) {
            return new Lesson[size];
        }
    };

    public void setContext(Context context) {
        this.context = context;
    }

    public void addNote(Note note) {
        getNotes().add(note);

        if (getId() == 0) setId(System.currentTimeMillis());

        DBHelper dbHelper = DBHelper.getDbHelper(context);
        dbHelper.addNote(this, note);
    }

    public void removeNote(Note note) {
        getNotes().remove(note);

        DBHelper dbHelper = DBHelper.getDbHelper(context);
        dbHelper.removeNote(this, note);
    }


    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}