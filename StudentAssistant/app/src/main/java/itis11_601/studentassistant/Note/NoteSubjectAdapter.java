package itis11_601.studentassistant.Note;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import itis11_601.studentassistant.ArchiveActivity;
import itis11_601.studentassistant.Lesson;
import itis11_601.studentassistant.R;
import itis11_601.studentassistant.Subject.SubjectOverviewActivity;

/**
 * Created by пользователь on 08.04.2017.
 */

public class NoteSubjectAdapter extends BaseAdapter {
    private ArrayList<Note> notes;
    SubjectOverviewActivity activity;
    ArchiveActivity archiveActivity;
    LayoutInflater inflater;
    private SimpleDateFormat noteFormat = new SimpleDateFormat("yyyy/mm/dd hh:mm");

    public NoteSubjectAdapter(SubjectOverviewActivity context, ArrayList<Note> notes) {
        this.activity = context;
        this.notes = notes;
        inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public NoteSubjectAdapter(ArchiveActivity context, ArrayList<Note> notes) {
        this.archiveActivity = context;
        this.notes = notes;
        inflater = (LayoutInflater) this.archiveActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Object getItem(int position) {
        return notes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        Note note = (Note) getItem(position);
        if (activity != null) {
            view = inflater.inflate(R.layout.subject_item, parent, false);
            ((TextView) view.findViewById(R.id.subject)).setText(note.getTitle());
            ((ImageButton) view.findViewById(R.id.imageButton)).setFocusable(false);
            ((ImageButton) view.findViewById(R.id.imageButton)).setId(position);
        } else if (archiveActivity != null) {
            view = inflater.inflate(R.layout.archive_note_item, parent, false);
            ((TextView) view.findViewById(R.id.title)).setText(note.getTitle());
            ((TextView) view.findViewById(R.id.discript)).setText(note.getDiscript());
            String s = noteFormat.format(((Lesson) note.getTags()).getDateStart().getTime());
            ((TextView) view.findViewById(R.id.date)).setText(s);
            ((TextView) view.findViewById(R.id.subject)).setText(note.getTags().getName());
            ((CheckBox) view.findViewById(R.id.checkBox)).setId(position);
        }

        return view;
    }
}