package itis11_601.studentassistant.Note;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

public class Note implements Parcelable, Serializable{

    private String title;
    private String discript;
    private NoteHandler tags;
    private long id = 0;

    public Note(String title, String discript, NoteHandler tags) {
        this.title = title;
        this.discript = discript;
        this.tags = tags;
    }

    public Note(String title, String discript) {
        this.title = title;
        this.discript = discript;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Note(String discript) {
        this.discript = discript;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscript() {
        return discript;
    }

    public void setDiscript(String discript) {
        this.discript = discript;
    }

    public NoteHandler getTags() {
        return tags;
    }

    public void setTags(NoteHandler tags) {
        this.tags = tags;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(discript);
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {

        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[0];
        }
    };

    private Note(Parcel parcel) {
        this.title = parcel.readString();
        this.discript = parcel.readString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Note note = (Note) o;

        if (title != null ? !title.equals(note.title) : note.title != null) return false;
        return discript != null ? discript.equals(note.discript) : note.discript == null;

    }

}
