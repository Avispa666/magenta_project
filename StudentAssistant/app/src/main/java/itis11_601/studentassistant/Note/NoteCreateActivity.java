package itis11_601.studentassistant.Note;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.Event;
import itis11_601.studentassistant.NotificationService.DeferredNotificationBuilder;
import itis11_601.studentassistant.R;
import itis11_601.studentassistant.Subject.Subject;

/**
 * Created by @lucky.mz on 31.03.2017.
 */

public class NoteCreateActivity extends DrawerActivity implements View.OnClickListener {

    private EditText title;
    private EditText discript, time, date;
    private Button okButton;
    private Note note;
    private Subject subject;
    private Event event;
    private Calendar remember = Calendar.getInstance();
    private int DIALOG_TIME = 1;
    private int DIALOG_DATE = 2;
    private CheckBox checkBox;
    private TextView tv;

    int myHour = remember.get(Calendar.HOUR_OF_DAY);
    int myMinute = remember.get(Calendar.MINUTE);
    int myYear = 2017;
    int myMonth = remember.get(Calendar.MONTH);
    int myDay = remember.get(Calendar.DAY_OF_MONTH);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_create_activity);
    }

    @Override
    protected void onFragmentCreated() {
        okButton = (Button) findViewById(R.id.okButton);
        okButton.setOnClickListener(this);
        //remember = Calendar.getInstance();

        title = (EditText) findViewById(R.id.title_create);
        discript = (EditText) findViewById(R.id.descript_create);
        time = (EditText) findViewById(R.id.time);
        date = (EditText) findViewById(R.id.date);
        checkBox = (CheckBox) findViewById(R.id.checkBox2);
        tv = (TextView) findViewById(R.id.dd);
        time.setVisibility(View.INVISIBLE);
        date.setVisibility(View.INVISIBLE);
        tv.setVisibility(View.INVISIBLE);

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_DATE);
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_TIME);
            }
        });

        note = getIntent().getParcelableExtra("Note");

        if (note != null) {
            Log.d("SonyaNote", "i get " + note.getTitle());
            title.setText(note.getTitle());
            discript.setText(note.getDiscript());
        }
    }


    protected Dialog onCreateDialog(int id){
        if(id == DIALOG_TIME) {
            TimePickerDialog tpd = new TimePickerDialog(this, myCallBack, remember.HOUR_OF_DAY, remember.MINUTE, true);
            return tpd;
        }
        if (id == DIALOG_DATE) {
            DatePickerDialog dpd = new DatePickerDialog(this, myCallBack2, remember.YEAR, remember.MONTH, remember.DAY_OF_MONTH);
            return dpd;
        }

        return super.onCreateDialog(id);
    }

    TimePickerDialog.OnTimeSetListener myCallBack = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            myHour = hourOfDay;
            myMinute = minute;
            remember.set(myYear, myMonth, myDay, myHour, myMinute);
            //open = true;

            TimeVisible();;
        }
    };

    DatePickerDialog.OnDateSetListener myCallBack2 = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            Log.d("SonyaNoti", year + "  " + dayOfMonth);
            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;
            remember.set(myYear, myMonth, myDay, myHour, myMinute);
            //open = true;
            TimeVisible();
        }
    };

    public void TimeVisible(){
        SimpleDateFormat day = new SimpleDateFormat("HH:mm");
        SimpleDateFormat yea = new SimpleDateFormat("dd/MM/yyyy");

        time.setText(day.format(remember.getTime()));
        date.setText(yea.format(remember.getTime()));
    }

    public void onClickCheck(View v) {
        if (checkBox.isChecked()) {
            time.setVisibility(View.VISIBLE);
            date.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
        }
        else{
            time.setVisibility(View.INVISIBLE);
            date.setVisibility(View.INVISIBLE);
            tv.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();
        subject = getIntent().getParcelableExtra("Subject");
//        Log.d("SonyaSub", "I get   in note " + subject.getName());
        event = (Event) getIntent().getSerializableExtra("Event");
        long notePos = getIntent().getLongExtra("NotePosition", 0);
//        Log.d("SonyaEventNote", String.valueOf(event.getNotes()));
//        Log.d("SonyaSub", "I get   in note " + subject.getName());

        switch (v.getId()) {
            case R.id.okButton:
                if (title.getText().toString().equals("")) {
                    Toast.makeText(NoteCreateActivity.this, "Название не может быть пустым", Toast.LENGTH_SHORT).show();
                    break;
                }


                Log.d("SonyaNoti", new SimpleDateFormat("yyyy/MM/dd HH:mm").format(remember.getTime()));
                Note newNote = new Note(title.getText().toString(), discript.getText().toString());
                Log.d("SonyaNote", "after create position ");
                boolean create = note == null;

                if (subject != null) {
                    subject.setContext(this);
                    if (create) subject.addNote(newNote);
                    else subject.updateNote(note, newNote);
                }
                if (event != null) {
                    event.setContext(this);
                    //if (create) event.addNote(newNote);
                    //event.getNotes().set((int) notePos, newNote);
                    Log.d("SonyaNote", "in create update event last: " + note.getTitle() + "   new: " + newNote.getTitle());
                    event.updateNote(note, newNote);
                }

                note = newNote;
                if (checkBox.isChecked()) {
                note.setId(System.currentTimeMillis());
                DeferredNotificationBuilder noti = new DeferredNotificationBuilder(this, NoteCreateActivity.class, title.getText().toString(),
                        discript.getText().toString(), 0, remember.getTimeInMillis(), (int) note.getId());
                noti.create();
            }
                Log.d("SonyaNote", String.valueOf(note));
                i.putExtra("Note", (Parcelable) note);
                //i.putExtra("NoteSerializable", (Serializable) note);
//                Log.d("SonyaEvent", String.valueOf(event) + "  " + event.getNotes().get(1));
                i.putExtra("position", getIntent().getLongExtra("position", 5));
                i.putExtra("Event", (Parcelable) event);
                setResult(RESULT_OK, i);
                finish();
                break;
        }
    }

}
