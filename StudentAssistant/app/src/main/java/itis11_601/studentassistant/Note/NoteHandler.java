package itis11_601.studentassistant.Note;

import java.util.ArrayList;

public abstract class NoteHandler {
    public ArrayList<Note> notes;

    public NoteHandler(ArrayList<Note> notes) {
        this.notes = notes;
    }

    public NoteHandler() {
    }

    public void addNote(Note note) {
        notes.add(note);
    }

    public void removeNote(Note note) {
        notes.remove(note);
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }

    public void setNotes(ArrayList<Note> notes) {
        this.notes = notes;
    }

    public abstract String getName();
}
