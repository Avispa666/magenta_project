package itis11_601.studentassistant.Configuration;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;

public class ConfigActivity extends DrawerActivity implements View.OnClickListener {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
    }


    @Override
    protected void onFragmentCreated() {

        Button gen = (Button) findViewById(R.id.general_config);
        gen.setOnClickListener(this);
        Button natife = (Button) findViewById(R.id.natife);
        natife.setOnClickListener(this);
        Button data = (Button) findViewById(R.id.data_conf);
        data.setOnClickListener(this);
        Button info = (Button) findViewById(R.id.inform);
        info.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.general_config:
                startActivity(new Intent(this, ConfigGeneralActivity.class));
                break;
            case R.id.natife:
                startActivity(new Intent(this, ConfigNatifeActivity.class));
                break;
            case R.id.data_conf:
                startActivity(new Intent(this,ConfigDatalActivity.class));
                break;
            case R.id.inform:
                startActivity(new Intent(this, ConfigInfoActivity.class));
        }
    }
}


/**private final String NAME = "Настройки";
 private ListView configList;
 private DBHelper dbHelper;
 dbHelper = DBHelper.getDbHelper(this);
 final ArrayList<String> configTitles = new ArrayList<>();
 if (getIntent().getBooleanExtra("mainAct", true)) {
 configTitles.add("Основные");
 configTitles.add("Уведомления и беззвучный режим");
 configTitles.add("Данные");
 configTitles.add("О программе");
 }
 else {
 configTitles.add(getIntent().getStringExtra("selectedItem"));
 }
 getSupportActionBar().setTitle(NAME);
 configList = (ListView) findViewById(R.id.configList);
 ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, configTitles);
 configList.setAdapter(adapter);
 configList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
@Override
public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
Intent i;
switch(position) {
case 0:
if (!getIntent().getBooleanExtra("mainAct", true)) {
dbHelper.clearDB();
}
break;
case 2:
i = new Intent(ConfigActivity.this, ConfigActivity.class);
i.putExtra("selectedItem", "Сброс данных");
i.putExtra("mainAct", false);
startActivity(i);
break;
}
}
});
 */
