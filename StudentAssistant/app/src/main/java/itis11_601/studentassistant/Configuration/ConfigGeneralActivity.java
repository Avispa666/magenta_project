package itis11_601.studentassistant.Configuration;

import android.os.Bundle;

import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;


public class ConfigGeneralActivity extends DrawerActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_config);
    }

    @Override
    protected void onFragmentCreated() {

    }
}
