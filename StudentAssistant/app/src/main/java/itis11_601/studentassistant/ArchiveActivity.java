package itis11_601.studentassistant;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.Note.NoteSubjectAdapter;
import itis11_601.studentassistant.Subject.Subject;

/**
 * Created by пользователь on 09.04.2017.
 */

public class ArchiveActivity extends DrawerActivity {
    private TextView emptyText;
    private FloatingActionButton fab;
    private static final String NAME = "Архив";
    private DBHelper dbHelper;
    private ListView list;
    private Subject subject;
    private ArrayList<Note> notes;
    private NoteSubjectAdapter adapter;
    private ArrayList<Note> checked;
    private DateFormat noteFormat = new SimpleDateFormat("yyyy/MM/dd");


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subject_layout);
    }

    @Override
    protected void onFragmentCreated() {
        checked = new ArrayList<>();
        dbHelper = DBHelper.getDbHelper(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        fab.setBackgroundColor(Color.BLUE);
        fab.setImageResource(R.drawable.ic_delete_forever_white_24dp);
        list = (ListView) findViewById(R.id.list);
        emptyText = (TextView) findViewById(R.id.emptyText);

        subject = getIntent().getParcelableExtra("Subject");
        notes = dbHelper.getArchiveNote(subject, new GregorianCalendar());
        Log.d("SonyaArchive", "my size = " + notes.size());
        Calendar de = new GregorianCalendar(2016, Calendar.AUGUST, 15);
        final SimpleDateFormat note = new SimpleDateFormat("yyyy/mm/dd");
        Calendar ds = new GregorianCalendar();
        try {

            ds.setTime(note.parse("2016/10/13"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        notes.add(new Note("Test1", "i testing",new Lesson("Math", "very hard" , ds, de, subject, 0)));
        dbHelper.addNote((Lesson)notes.get(0).getTags(),notes.get(0));
        //notes.add(new Note("Test2", "i continue", new Lesson("Diskr", "", de, de, subject, 0)));
        updateList(notes);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = checked.size() - 1; i >= 0; i--) {
                    Log.d("SonyaArchive", "I delete " + i);
                    dbHelper.removeNote((Lesson)checked.get(i).getTags(), checked.get(i));
                    notes.remove(checked.get(i));
                    checked.remove(i);
                }
                updateList(notes);
                fab.setVisibility(View.INVISIBLE);
            }
        });


    }


    private void updateList(ArrayList<Note> notes) {
        if (adapter != null) adapter = null;
        adapter = new NoteSubjectAdapter(this, notes);
        list.setAdapter(adapter);
        emptyListState();

    }

    private void emptyListState() {
        if (list.getCount() == 0) {
            emptyText.setVisibility(View.VISIBLE);
        } else {
            emptyText.setVisibility(View.INVISIBLE);
        }
    }

    public void checked(View view) {

        CheckBox checkBox = (CheckBox) view;
        if (checked.contains(notes.get(checkBox.getId()))) {
            checked.remove(notes.get(checkBox.getId()));
        }
        else {
            checked.add(notes.get(checkBox.getId()));
        }
        if (checked.size() == 0) {
            fab.setVisibility(View.INVISIBLE);
        }
        else fab.setVisibility(View.VISIBLE);
        checkBox.setText("");
        Log.d("SonyaArchive", "yeeeee, i check");

    }

}
