package itis11_601.studentassistant.Assistant;

import android.app.Activity;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * Created by Vladislav on 13.04.2017.
 */
public class AssistantInfoWriter
{
    BufferedWriter bw;
    Activity activity;
    public AssistantInfoWriter(String filename, Activity activity){
        this.activity = activity;
        try {
            // отрываем поток для записи
            bw = new BufferedWriter(new OutputStreamWriter(
                    activity.openFileOutput(filename, activity.MODE_PRIVATE)));
            System.out.println("Dddddddda");
            // пишем данные

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    public void writeAssistantInfo(String name, String photoLink){
        try {
            bw.write(name + "\r\n");
            bw.write(photoLink);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // закрываем поток

    }
}
