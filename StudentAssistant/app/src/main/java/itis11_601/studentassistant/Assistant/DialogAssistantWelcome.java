package itis11_601.studentassistant.Assistant;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;

/**
 * Created by Vladislav on 09.04.2017.
 */
public class

DialogAssistantWelcome extends DialogFragment implements View.OnClickListener {

    final String LOG_TAG = "myLogs";
    private EditText nameEditText;
    private Context cont;
    private Switch switchView;
    private ImageView imageView;
    private String curImageName = "icon_man";
    private NavigationView navigationView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Создание помощника");

        Log.d(LOG_TAG,"d");
        View v = inflater.inflate(R.layout.dialog_assistant_welcome, null);
        v.findViewById(R.id.btnOk).setOnClickListener(this);
        switchView = (Switch) v.findViewById(R.id.switch_sex);
        imageView = (ImageView) v.findViewById(R.id.image_sex);
        imageView.setImageResource(R.drawable.icon_man);
        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    curImageName = "icon_woman";
                    imageView.setImageResource(R.drawable.icon_woman);
                }
                else{
                    curImageName = "icon_man";
                    imageView.setImageResource(AssistantInfoReader.getResId("icon_man", R.drawable.class));

                }
            }
        });

        nameEditText = (EditText) v.findViewById(R.id.edit_Text_Name);

        return v;
    }
    public void setCont(Context context){
        cont = context;
    }

    public void onClick(View v) {
        String name = nameEditText.getText().toString();

        if (name.equals("")) {
            Toast.makeText(cont, "plz enter the name ", Toast.LENGTH_SHORT).show();
            return;
        }
        AssistantInfoWriter assistantInfoWriter = new AssistantInfoWriter("aboutAssistant.txt", getActivity());
        assistantInfoWriter.writeAssistantInfo(name,curImageName);
        TextView tv = (TextView) navigationView.findViewById(R.id.assistant_name);
        tv.setText(name);
        ImageView imageView = (ImageView) navigationView.findViewById(R.id.img);
        imageView.setImageResource(AssistantInfoReader.getResId(curImageName, R.drawable.class));;


        //TextView header = (TextView) v2.findViewById(R.id.assistant_name);


        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }


    public void setNavigationView(NavigationView navigationView) {
        this.navigationView = navigationView;
    }
}