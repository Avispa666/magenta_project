package itis11_601.studentassistant.Assistant;

import android.app.Activity;

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;

/**
 * Created by Vladislav on 13.04.2017.
 */
public class AssistantInfoReader {
    BufferedReader br;
    Activity activity;
    public AssistantInfoReader(String fileName, Activity activity){
        try {
            br = new BufferedReader(new InputStreamReader(
                    activity.openFileInput(fileName)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public String[] read(){
        String[] out = new String[2];
        String str;
        int i = 0;
        if(br == null)
            return out;
        try {
            while((str = br.readLine()) != null){
                out[i] = str;
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  out;
    }

    public static int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
