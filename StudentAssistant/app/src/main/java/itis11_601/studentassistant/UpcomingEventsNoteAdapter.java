package itis11_601.studentassistant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.Note.NoteCreateActivity;

/**
 * Created by User on 06.04.2017.
 */

public class UpcomingEventsNoteAdapter extends RecyclerView.Adapter<UpcomingEventsNoteAdapter.ViewHolder> {

    private ArrayList<Note> notes;
    private Activity context;
    private LayoutInflater in;
    private Event event;
    private long id;

    public UpcomingEventsNoteAdapter(ArrayList<Note> notes, Activity context, Event event) {
        this.context = context;
        this.notes = notes;
        this.event = event;
        in = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = in.inflate(R.layout.upcoming_events_notes, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setId(position);
        Note note = notes.get(position);
        holder.text.setText(note.getTitle());
        holder.note = note;
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        CardView cv;
        Note note;

        public ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.note_cv);
            text = (TextView) itemView.findViewById(R.id.note_title);
            //final long id = itemView.getId();

            Log.d("SonyaEv", String.valueOf(id));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("SonyaEventNote", String.valueOf(v.getId()));
                    Intent i = new Intent(context, NoteCreateActivity.class);
                    Note newNote = new Note(text.getText().toString(), note.getDiscript());
                    i.putExtra("Note", (Parcelable) newNote);
                    i.putExtra("Event", (Serializable) event);
                    Log.d("SonyaUp", id + " event ");
                    i.putExtra("position", id);
                    context.startActivityForResult(i, 7);

                }
            });

            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add(Menu.NONE, 101, Menu.NONE,  "Удалить");

                }


                public boolean onContextItemSelected(MenuItem item) {
                   switch (item.getItemId()) {
                       case 101:
                           event.removeNote(note);
                           break;
                   }
                    return true;
                }
            });
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
