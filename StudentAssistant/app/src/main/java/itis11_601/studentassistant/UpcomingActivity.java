package itis11_601.studentassistant;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import itis11_601.studentassistant.Assistant.DialogAssistantWelcome;

/**
 * Created by 1 on 18.03.2017.
 */

public class UpcomingActivity extends DrawerActivity {
    final String LOG_TAG = "myLogs";
    private static final String TAG = "UpcomingActivity";

    private TextView emptyText;
    public static final String NAME = "Предстоящие события";
    private DialogAssistantWelcome dialogWelcome;
    private DBHelper dbHelper;
    private ArrayList<Event> events;
    private UpcomingEventsAdapter upcomingEventsAdapter;
    private RecyclerView rv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upcoming_events);
        Log.d(LOG_TAG, "HER");


        // check whether an app opens firstTime
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);//this==context
        if(!prefs.contains("FirstTime")) {
            //Other dialog code
            Log.d(LOG_TAG, "ddd");
            dialogWelcome = new DialogAssistantWelcome();
            dialogWelcome.setCont(this);
            dialogWelcome.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
            //dialogWelcome.setDrawerActivity(super);
            dialogWelcome.setNavigationView(super.getNvDrawer());

            dialogWelcome.show(getFragmentManager(), "dialogWelcome");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("FirstTime", true);
            editor.commit();
        }

    }

    @Override
    protected void onFragmentCreated() {
        emptyText = (TextView) findViewById(R.id.emptyText);
        getSupportActionBar().setTitle(NAME);

        rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        dbHelper = DBHelper.getDbHelper(this);

        events = dbHelper.getAllEvents();
        upcomingEventsAdapter = new UpcomingEventsAdapter(this, events);
        rv.setAdapter(upcomingEventsAdapter);
        emptyListState();
    }

    public void eventItem(View view) {
        Log.d("SonyaButton", "It work");
        final ImageButton ib = (ImageButton)view;
        PopupMenu popup = new PopupMenu(this, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.subject_menu, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        if (events.get(ib.getId()) instanceof Lesson)
                            dbHelper.removeEvent((Lesson)events.get(ib.getId()));
                        else
                            dbHelper.removeEvent(events.get(ib.getId()));
                        events.remove(ib.getId());
                        Log.d("SonyaUp", "i click " + ib.getId());
                        //айдишник кидает выход за пределы массива
                        //TODO проверить метод после подкл
                        updateUpcomingActivity(events);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    public void updateUpcomingActivity(ArrayList<Event> events) {
        if (upcomingEventsAdapter != null) upcomingEventsAdapter = null;
        upcomingEventsAdapter = new UpcomingEventsAdapter(this, events);
        rv.setAdapter(upcomingEventsAdapter);
        emptyListState();
    }

    private void emptyListState() {
        if (upcomingEventsAdapter.getItemCount() != 0) {
            emptyText.setVisibility(View.INVISIBLE);
        } else {
            emptyText.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 7) {
                Event newEvent = intent.getParcelableExtra("Event");
                /////check work
                Log.d("SonyaEvent", "i get in upc " + newEvent + "    " + intent.getLongExtra("position", 5));
                events.set(intent.getIntExtra("positin", 0), newEvent);
                updateUpcomingActivity(events);
            }
        }
    }







}
