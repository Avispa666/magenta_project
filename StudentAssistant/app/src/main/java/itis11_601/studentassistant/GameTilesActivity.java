package itis11_601.studentassistant;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import itis11_601.studentassistant.GameTiles.GameTiles;

public class GameTilesActivity extends AppCompatActivity {
    private GameTiles game;
    private boolean[][] field;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new FieldView(this));
//        setContentView(R.layout.activity_game_tiles);
        game = GameTiles.start();
    }

    Handler mHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(GameTilesActivity.this);
            builder.setTitle("It's fuckin unbelieveable!")
                    .setMessage("How you did it, dude?")
                    .setCancelable(false)
                    .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onBackPressed();
                        }
                    });
//            builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    dialogInterface.cancel();
//                }
//            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    };

    public class FieldView extends SurfaceView implements SurfaceHolder.Callback {

        private static final int MARGIN = 100;
        private DrawThread drawThread;
        private GestureDetector detector;
        private int counter;
        private boolean clickable;
        private boolean getFieldOnce;
        private boolean timePicked;
        private int squareSize;

        public FieldView(Context context) {
            super(context);
            getHolder().addCallback(this);
            clickable = false;
            timePicked = false;
            this.detector = new GestureDetector(context, new MyGestureListener());
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
            getHolder().addCallback(this);
            clickable = false;
            timePicked = false;
            this.detector = new GestureDetector(context, new MyGestureListener());
        }

        public FieldView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            getHolder().addCallback(this);
            clickable = false;
            timePicked = false;
            this.detector = new GestureDetector(context, new MyGestureListener());
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            detector.onTouchEvent(event);
            return true;
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            drawThread = new DrawThread(getHolder());
            drawThread.setRunning(true);
            drawThread.start();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            boolean retry = true;
            drawThread.setRunning(false);
            while (retry) {
                try {
                    drawThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        class DrawThread extends Thread {

            private static final int timeGap = 3000;
            private static final String TAG = "DrawThread";
            private boolean running = false;
            private long time = 0;
            private Paint white;
            private Paint black;
            private Paint line;
            private SurfaceHolder surfaceHolder;

            DrawThread(SurfaceHolder surfaceHolder) {
                white = new Paint();
                white.setAntiAlias(true);
                white.setDither(true);
                white.setColor(Color.WHITE);
                white.setStrokeWidth(5);
                white.setStyle(Paint.Style.FILL);
                black = new Paint();
                black.setAntiAlias(true);
                black.setDither(true);
                black.setColor(Color.BLACK);
                black.setStrokeWidth(5);
                black.setStyle(Paint.Style.FILL);
                line = new Paint();
                line.setAntiAlias(true);
                line.setDither(true);
                line.setColor(Color.RED);
                line.setStrokeWidth(5f);
                line.setStyle(Paint.Style.FILL);
                this.surfaceHolder = surfaceHolder;
                counter = 0;
                getFieldOnce = true;
            }

            void setRunning(boolean running) {
                this.running = running;
            }

            @Override
            public void run() {
                Canvas canvas;
                while (running) {
                    if (getFieldOnce) {
                        field = game.getField();
                        if (field.length == 8) {
                            mHandler.sendEmptyMessage(0);
                        }
                        Log.d(TAG, "run: getting field");
                        clickable = false;
                        getFieldOnce = false;
                    }
                    canvas = null;
                    try {
                        canvas = surfaceHolder.lockCanvas(null);
                        if (canvas == null)
                            continue;
                        canvas.drawColor(Color.LTGRAY);
                        int x = 0, y = MARGIN;
                        squareSize = Math.min(canvas.getHeight(), canvas.getWidth())
                                / field.length;
                        for (int i = 0; i < field.length; i++) {
                            for (int j = 0; j < field[i].length; j++) {
                                canvas.drawRect(x, y, x + squareSize, y + squareSize,
                                        field[j][i] ? white : black);
                                x += squareSize;
                            }
                            x = 0;
                            y += squareSize;
                        }
                        y = MARGIN;
                        x = 0;
                        for (int j = 0; j < field.length + 1; j++) {
                            canvas.drawLine(x, y, x, squareSize * field.length + MARGIN, line);
                            x += squareSize;
                        }
                        y = MARGIN;
                        x = 0;
                        for (int i = 0; i < field.length + 1; i++) {
                            canvas.drawLine(x, y, squareSize * field.length, y, line);
                            y += squareSize;
                        }
                        if (!timePicked) {
                            time = System.currentTimeMillis();
                            timePicked = true;
                        }

                    } finally {
                        if (Math.abs(System.currentTimeMillis() - time - timeGap) < 10) {
                            field = new boolean[field.length][field.length];
                            Log.d(TAG, "run: black board");
                            counter = 0;
                            clickable = true;
                        }
                        if (canvas != null) {
                            surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    }
                }
            }
        }

        private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (e.getY() > MARGIN + squareSize * field.length) return true;
                int x = (int) e.getX() / squareSize;
                int y = (int) (e.getY() - MARGIN) / squareSize;
                if (clickable && counter < field.length) {
                    field[x][y] = true;
                    ++counter;
                }
                if (counter == field.length) {
                    Log.d("GstListener", "onSingleTapConfirmed: checking answer");
                    game.submitAnswer(field);
                    getFieldOnce = true;
                    timePicked = false;
                    counter = 0;
                }
                return true;
            }
        }
    }
}
