package itis11_601.studentassistant;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import itis11_601.studentassistant.Assistant.AssistantInfoReader;

import itis11_601.studentassistant.Statistic.SleepStats.SleepStatsHelper;
import itis11_601.studentassistant.Configuration.ConfigActivity;
import itis11_601.studentassistant.Statistic.StatisticActivity;

import itis11_601.studentassistant.Subject.SubjectsActivity;
import itis11_601.studentassistant.timetable.ScheduleActivity;

public abstract class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "Drawer_activity";
    private DrawerLayout mDrawer;
    private SleepStatsHelper helper;
//    private Toolbar toolbar;
//
//    public NavigationView getNvDrawer() {
//        return nvDrawer;
//    }

    protected Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private MyFragment includedFragment;
    private IconOnClickListener listener;
    private ImageView notificationDot;

    private class IconOnClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.img:
                    startActivity(new Intent(DrawerActivity.this, AssistantMainActivity.class));
            }
        }
    }


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(R.layout.drawer_main_layout);
        NavigationView navigationView = (NavigationView) super.findViewById(R.id.nvView);
        View headerLayout = navigationView.getHeaderView(0);
        TextView assistantNameView = (TextView) headerLayout.findViewById(R.id.assistant_name);
        ImageView assistantImageView = (ImageView) headerLayout.findViewById(R.id.img);
        notificationDot = (ImageView) headerLayout.findViewById(R.id.notification_dot);
        listener = new IconOnClickListener();
        helper = new SleepStatsHelper(this);
        updateNotifications();
        assistantImageView.setOnClickListener(listener);
        Log.d(TAG, "setContentView: onclck");
        toolbar = (Toolbar) super.findViewById(R.id.toolbar);

        assistantNameView.setText("");
        //
        //View v = navigationView.getHeaderView(0);



        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout) super.findViewById(R.id.drawer_layout);
        // AssistantInfo
        AssistantInfoReader assistantInfoReader = new AssistantInfoReader("aboutAssistant.txt", this);
        String [] assistantInfo = assistantInfoReader.read();
        if(
                assistantInfo[0] != null && assistantInfo[1] != null){
        assistantNameView.setText(assistantInfo[0]);
        System.out.println(assistantInfo[1]);
        assistantImageView.setImageResource(AssistantInfoReader.getResId(assistantInfo[1], R.drawable.class));;
        }
        else{
            System.out.println("file pust");
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        toolbarInit();
        mDrawer = (DrawerLayout) super.findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, mDrawer, toolbar,
                R.string.drawer_open, R.string.drawer_closed) {

            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_SETTLING) {
                    Log.d(TAG, "onDrawerStateChanged: ");
                    updateNotifications();
                    invalidateOptionsMenu();
                }
            }

        };
        drawerToggle.syncState();
        setIncludeLayout(layoutResID);
    }

    private void updateNotifications() {
        if (helper.isPickedStatsToday()) notificationDot.setVisibility(View.GONE);
        else notificationDot.setVisibility(View.VISIBLE);
    }

    protected void toolbarInit() {
        toolbar = (Toolbar) super.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Nullable
    @Override
    public View findViewById(@IdRes int id) {
        if (includedFragment == null) throw new UnsupportedOperationException("Calling findView before fragment included view created");
        View res = includedFragment.findViewById(id);
        if (res == null) return super.findViewById(id);
        return res;
    }

    private void setIncludeLayout(@LayoutRes int layoutResID) {
        MyFragment fragment = new MyFragment();
        fragment.setActivity(this);
        Bundle args = new Bundle();
        args.putInt(MyFragment.RESOURCE_ID, layoutResID);
        fragment.setArguments(args);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.flContent, fragment)
                .commit();
        nvDrawer = (NavigationView) super.findViewById(R.id.nvView);
        includedFragment = fragment;
        nvDrawer.setNavigationItemSelectedListener(this);
    }

    public DrawerLayout getMDrawer() {
        return mDrawer;
    }

    public static class MyFragment extends Fragment {
        public static final String RESOURCE_ID = "REsourCE_ID";
        private DrawerActivity activity;
        private View includedView;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container,
                                 Bundle savedInstanceState) {

            includedView = inflater.inflate((int) getArguments().get(RESOURCE_ID),
                    container,
                    false);

            activity.onFragmentCreated();
            return includedView;
        }

        public View findViewById(@IdRes int id) {
            if (includedView == null) return null;
            return includedView.findViewById(id);
        }

        public void setActivity(DrawerActivity activity) {
            this.activity = activity;
        }
    }

    abstract protected void onFragmentCreated();


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_schedule_item:
                startActivity(new Intent(this, ScheduleActivity.class));
                break;
            case R.id.nav_subjects_item:
                startActivity(new Intent(this, SubjectsActivity.class));
                break;
            case R.id.nav_upcoming:
                startActivity(new Intent(this, UpcomingActivity.class));
                break;
            case R.id.nav_games_item:
                startActivity(new Intent(this, GamesActivity.class));
                break;
            case R.id.nav_statistics_item:
                startActivity(new Intent(this, StatisticActivity.class));
                break;
            case R.id.nav_config:
                startActivity(new Intent(this, ConfigActivity.class));
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public NavigationView getNvDrawer() {
        return nvDrawer;
    }
}

