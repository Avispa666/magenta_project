package itis11_601.studentassistant;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;

public class Day {
    private ArrayList<Event> events;
    private Calendar date;

    public Day(ArrayList<Event> events) {
        this.events = events;
    }

    public Day(Context context, Calendar date) {
        this.date = date;
    }

    public Day(Calendar date) {
        this.date = date;
    }

    public void getAllEventsFromDB() {
        //finish
    }

    public void addEvent(Event event) {
        events.add(event);
    }

    public void pushToDB() {
        //finish
    }

    public void removeEvent(Event event) {
        events.remove(event);
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
