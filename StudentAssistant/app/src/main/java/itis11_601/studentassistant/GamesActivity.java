package itis11_601.studentassistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.View;

import itis11_601.studentassistant.GamePairs.GamePairsActivity;
import itis11_601.studentassistant.UnnecessaryGame.UnnecessaryActivity;


/**
 * Created by 1 on 25.03.2017.
 */

public class GamesActivity extends DrawerActivity implements View.OnClickListener {

    public static final String NAME = "Тесты";
    private CardView[] cardViews;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_game_layout);
    }

    @Override
    protected void onFragmentCreated() {
        cardViews = new CardView[3];
        cardViews[0] = (CardView) findViewById(R.id.card1);
        cardViews[1] = (CardView) findViewById(R.id.card2);
        cardViews[2] = (CardView) findViewById(R.id.card3);
        cardViews[0].setOnClickListener(this);
        cardViews[1].setOnClickListener(this);
        cardViews[2].setOnClickListener(this);
        super.getSupportActionBar().setTitle(NAME);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card1:
                startActivity(new Intent(this, GamePairsActivity.class));
                break;
            case R.id.card2:
                startActivity(new Intent(this, UnnecessaryActivity.class));
                break;
            case R.id.card3:
                startActivity(new Intent(this, GameTilesActivity.class));
                break;
        }
    }
}
