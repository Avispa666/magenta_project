package itis11_601.studentassistant;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import itis11_601.studentassistant.Statistic.SleepStats.SleepStatsHelper;

public class AssistantMainActivity extends DrawerActivity implements View.OnClickListener{
    private Button add;
    private Button deleteLast;
    private EditText inputField;
    private SleepStatsHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistant_main);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                helper.addValue(Integer.parseInt(inputField.getText().toString()));
                inputField.setText("");
                break;
            case R.id.deleteLast:
                helper.deleteLastHours();
                break;
        }
    }

    @Override
    protected void onFragmentCreated() {
        helper = new SleepStatsHelper(this);
        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(this);
        deleteLast = (Button) findViewById(R.id.deleteLast);
        deleteLast.setOnClickListener(this);
        inputField = (EditText) findViewById(R.id.hoursInsertField);
    }

}
