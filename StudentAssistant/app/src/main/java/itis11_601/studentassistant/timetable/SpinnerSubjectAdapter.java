package itis11_601.studentassistant.timetable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import itis11_601.studentassistant.Subject.Subject;

/**
 * Created by 1 on 13.04.2017.
 */

public class SpinnerSubjectAdapter extends ArrayAdapter {

    private ArrayList<Subject> subjects;
    private int resourseId;
    private LayoutInflater in;
    private Context context;

    public SpinnerSubjectAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resourseId = resource;
        this.subjects = (ArrayList<Subject>) objects;
        in = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public Subject getItem(int position) {
        return subjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = in.inflate(resourseId, null);
        }
        TextView text = (TextView) convertView;
        text.setText(subjects.get(position).getName());
        return text;
    }
}
