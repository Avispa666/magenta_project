package itis11_601.studentassistant.timetable;

import com.alamkanak.weekview.WeekViewEvent;
import com.alamkanak.weekview.WeekViewLoader;

import java.util.Calendar;
import java.util.List;

/**
 * Created by 1 on 12.04.2017.
 */

public class WeekLoader implements WeekViewLoader {

    private OnWeekChangeListener onWeekChangeListener;

    public WeekLoader(OnWeekChangeListener onWeekChangeListener) {
        this.onWeekChangeListener = onWeekChangeListener;
    }

    @Override
    public double toWeekViewPeriodIndex(Calendar instance) {
        return instance.get(Calendar.WEEK_OF_YEAR);
    }

    @Override
    public List<? extends WeekViewEvent> onLoad(int periodIndex) {
        return onWeekChangeListener.onWeekChanged(periodIndex);
    }

    public interface OnWeekChangeListener {
        List<? extends WeekViewEvent> onWeekChanged(int newWeek);
    }
}
