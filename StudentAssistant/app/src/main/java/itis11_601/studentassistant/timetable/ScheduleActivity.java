package itis11_601.studentassistant.timetable;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.Event;
import itis11_601.studentassistant.Lesson;
import itis11_601.studentassistant.R;

/**
 * Created by Максим on 16.03.2017.
 */
public class ScheduleActivity extends DrawerActivity implements WeekView.EventClickListener, WeekView.EventLongPressListener, WeekView.EmptyViewClickListener, WeekLoader.OnWeekChangeListener {

    private Calendar singleClickedTime;
    private WeekView weekView;
    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_WEEK_VIEW = 3;
    private int mWeekViewType = TYPE_WEEK_VIEW;
    private Spinner spinner;
    private String[] listOfSpinner;
    private List<WeekViewEvent> viewEvents;
    private List<Event> simpleEvents;
    private DBHelper db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_layout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.timetable_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_today:
                onNow();
                return true;
        }
        return false;
    }

    @Override
    protected void onFragmentCreated() {
        db = DBHelper.getDbHelper(this);
        //export();
        simpleEvents = db.getAllEvents();
        Log.e("Tags", simpleEvents.size() + "");
        weekView= (WeekView) findViewById(R.id.weekView);
        timetableInit();
        setupDateTimeInterpreter(false);
        viewEvents = new ArrayList<>();
        listOfSpinner = new String[] {
                "Неделя", "День"
        };
        weekView.setOnEventClickListener(this);
        weekView.setEmptyViewClickListener(this);
        weekView.setEventLongPressListener(this);
        weekView.setWeekViewLoader(new WeekLoader(this));
        spinnerInit();
        spinner.setSelection(0);
    }

    private void export() {
        Event changed = (Event) getIntent().getSerializableExtra("changedEvent");
        if (changed != null) {
            db.updateEvent(changed);
        }
    }

    private void spinnerInit() {
        ActionBar ab = getSupportActionBar();
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setVisibility(View.VISIBLE);
        ab.setDisplayShowTitleEnabled(false);
        SpinnerAdapter<String> adapter =  new SpinnerAdapter<>(this,
                R.layout.snipper_item, listOfSpinner);
        adapter.setDropDownViewResource(R.layout.snipper_dropdown);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        if (mWeekViewType == TYPE_WEEK_VIEW)
                            return;
                        mWeekViewType = TYPE_WEEK_VIEW;
                        timetableInit();
                        onNow();
                        break;
                    case 1:
                        if (mWeekViewType == TYPE_DAY_VIEW)
                            return;
                        mWeekViewType = TYPE_DAY_VIEW;
                        timetableInit();
                        onNow();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void timetableInit() {
        weekView.setShowDistinctWeekendColor(true);
        weekView.setShowNowLine(true);
        switch (mWeekViewType) {
            case TYPE_WEEK_VIEW:
                weekView.setNumberOfVisibleDays(7);

                // Lets change some dimensions to best fit the view.
                weekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                weekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 9, getResources().getDisplayMetrics()));
                weekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                break;
            case TYPE_DAY_VIEW:
                weekView.setNumberOfVisibleDays(1);

                // Lets change some dimensions to best fit the view.
                weekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                weekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                weekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                break;
        }
    }

    private void onNow() {
        Calendar c = Calendar.getInstance();
        weekView.goToHour(c.get(Calendar.HOUR_OF_DAY));
        weekView.goToToday();
    }

    private void setupDateTimeInterpreter(final boolean shortDate) {
        weekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                return hour + ":00";
            }
        });
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        // TODO: 12.04.2017 now
        Intent i = new Intent(this, EventActivity.class);
        Event e = getEvent(event);
        Log.e("SonyaEvent", "i put " + e.getName());
        i.putExtra("event", (Parcelable) e);
        startActivity(i);
    }

    private Event getEvent(WeekViewEvent event) {
        for (Event e : simpleEvents) {
            if (e.getId() == event.getId()) {
                return e;
            }
        }
        return null;
    }

    @Override
    public void onEventLongPress(final WeekViewEvent event, final RectF eventRect) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog
                .setTitle("Событие")
                .setMessage("Удалить событие?")
                .setPositiveButton("Удалить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Event e = getEvent(event);
                        Log.e("Tags", e.getName());
                        if (e instanceof Lesson) {
                            db.removeEvent((Lesson) e);
                        } else {
                            db.removeEvent(e);
                            Log.e("Tags", "here");
                        }
                        viewEvents.remove(event);
                        simpleEvents.remove(e);
                        finish();
                        startActivity(new Intent(ScheduleActivity.this, ScheduleActivity.class));
                        overridePendingTransition(0, 0);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public void onEmptyViewClicked(Calendar time) {
        if (singleClickedTime == null || !timeCompare(time, singleClickedTime)) {
            Toast.makeText(this, "Нажмите дважды, чтобы создать", Toast.LENGTH_SHORT).show();
        } else {
            Intent i = new Intent(this, EventActivity.class);
            i.putExtra("time", time);
            startActivity(i);
        }
        singleClickedTime = time;
    }

    private boolean timeCompare(Calendar t1, Calendar t2) {
        return t1.get(Calendar.DAY_OF_MONTH) == t2.get(Calendar.DAY_OF_MONTH) &&
                t1.get(Calendar.YEAR) == t2.get(Calendar.YEAR) &&
                t2.get(Calendar.MONTH) == t2.get(Calendar.MONTH);
    }

    @Override
    public List<? extends WeekViewEvent> onWeekChanged(int newWeek) {
        ArrayList<WeekViewEvent> newList = new ArrayList<>();
        for (Event e : simpleEvents) {
            if (e instanceof Lesson) {
                if (((Lesson) e).getPeriod() == 1) {
                    if (Math.abs(e.getDateStart().get(Calendar.WEEK_OF_YEAR) - newWeek) % 2 == 0) {
                        newList.add(toWeekViewEvent(e, newWeek));
                    }
                } else if (((Lesson) e).getPeriod() == 0) {
                    newList.add(toWeekViewEvent(e, newWeek));
                }
            } else {
                if (e.getDateStart().get(Calendar.WEEK_OF_YEAR) == newWeek) {
                    Log.e("SCF", e.getDateStart().getTime() + "");
                    newList.add(toWeekViewEvent(e, newWeek));
                }
            }
        }
        return newList;
    }

    public WeekViewEvent toWeekViewEvent(Event e, int newWeek) {
        Calendar c = (Calendar) e.getDateStart().clone();
        WeekViewEvent tmp = new WeekViewEvent();
        tmp.setName(e.getName());
        tmp.setId(e.getId());
        c.set(Calendar.WEEK_OF_YEAR, newWeek);
        tmp.setStartTime(c);
        c = (Calendar) e.getDateEnd().clone();
        c.set(Calendar.WEEK_OF_YEAR, newWeek);
        tmp.setEndTime(c);

        tmp.setColor(getResources().getColor(e.getColorId()));
        return tmp;
    }
}