package itis11_601.studentassistant.timetable;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by 1 on 08.04.2017.
 */

public class SpinnerAdapter<T> extends ArrayAdapter {

    private Context context;
    private int viewId;
    private T[] value;
    private LayoutInflater in;

    public SpinnerAdapter(Context context, int textViewResourceId, Object[] objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.viewId = textViewResourceId;
        this.value = (T[]) objects;
        in = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = in.inflate(viewId, null);
        }
        TextView tv = (TextView) convertView;
        tv.setText(value[position] + "");
        return convertView;
    }
}
