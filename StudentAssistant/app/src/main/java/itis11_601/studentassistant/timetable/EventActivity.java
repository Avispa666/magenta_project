package itis11_601.studentassistant.timetable;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Calendar;

import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.Event;
import itis11_601.studentassistant.Lesson;
import itis11_601.studentassistant.R;
import itis11_601.studentassistant.Subject.Subject;

/**
 * Created by 1 on 08.04.2017.
 */

public class EventActivity extends DrawerActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {

    private EditText name;
    private EditText discript;
    private Calendar currentBegin;
    private Calendar currentEnd;
    private Button begin;
    private Button end;
    private Spinner spinner;
    private Button create;
    private DBHelper db;
    private RadioGroup group;
    private String[] res = {
            "Event", "Lesson"
    };
    private ImageButton colorButton;
    private int[] colorSet;
    private int currentColor;
    private int type;
    public static final int EVENT = 1;
    public static final int LESSON = 2;
    public static final int EVERY_WEEK = 0;
    public static final int EVERY_TWO_WEEK_COUNT = 1;
    public static final String PUT_EVENT = "event";
    private int isWeekPeriod;
    private Spinner subjectsSpinner;
    private Subject subject;
    private ArrayList<Subject> array;
    private Button cancel;
    private int weekId;
    private int week2Id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_creation_layout);
    }


    @Override
    protected void onFragmentCreated() {
        Log.e("export", "begin");
        db = DBHelper.getDbHelper(this);
        Log.e("export", "Pechalko 0");
        array = db.getAllSubjects();
        Log.e("export", "Pechalko with subj");
        attributesInit();
        Log.e("export", "Pechalko 1");
        spinnerInit();
        Log.e("export", "Pechalko 2");
        subjectSpinnerInit();
        Log.e("export", "Pechalko 3");
        export();
        listenersInit();
        setTimeAsText();
        colorSetInit();
    }

    private void listenersInit() {
        group.setOnCheckedChangeListener(this);
        begin.setOnClickListener(this);
        colorButton.setOnClickListener(this);
        end.setOnClickListener(this);
        create.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    private void attributesInit() {
        name = (EditText) findViewById(R.id.event_create_text);
        begin = (Button) findViewById(R.id.time_edit_start);
        end = (Button) findViewById(R.id.time_edit_end);
        cancel = (Button) findViewById(R.id.cancel_new_event_button);
        group = (RadioGroup) findViewById(R.id.period_selector);
        colorButton = (ImageButton) findViewById(R.id.color_button);
        discript = (EditText) findViewById(R.id.event_discript);
        create = (Button) findViewById(R.id.create_new_event_button);
        subjectsSpinner = (Spinner) findViewById(R.id.subject_spinner);
        weekId = R.id.week_1;
        week2Id = R.id.week_2;
    }

    private void subjectSpinnerInit() {
        SpinnerSubjectAdapter adapter = new SpinnerSubjectAdapter(this, android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(R.layout.snipper_dropdown);
        subjectsSpinner.setAdapter(adapter);
        subjectsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (type == LESSON) {
                    subject = array.get(position);
                    name.setText(array.get(position).getName());
                    discript.setText("Ауд. " + array.get(position).getClassNum());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerInit() {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        spinner = (Spinner) toolbar.findViewById(R.id.spinner);
        SpinnerAdapter<String> adapter = new SpinnerAdapter<>(this,
                R.layout.snipper_item, res);
        adapter.setDropDownViewResource(R.layout.snipper_dropdown);
        spinner.setAdapter(adapter);
        spinner.setVisibility(View.VISIBLE);
        spinner.setOnItemSelectedListener(this);
    }

    private void export() {
        Intent intent = getIntent();
        if (intent.getParcelableExtra(PUT_EVENT) != null) {
            Event e = intent.getParcelableExtra(PUT_EVENT);
            currentBegin = (Calendar) e.getDateStart().clone();
            currentEnd = (Calendar) e.getDateEnd().clone();
            name.setText(e.getName());
            discript.setText(e.getDiscript());
            colorButton.setBackgroundColor(getResources().getColor(e.getColorId()));
            create.setText("Сохранить");
            spinner.setEnabled(false);
            if (e instanceof Lesson) {
                type = LESSON;
                spinner.setSelection(1);
                subjectsSpinner.setSelection(array.indexOf(((Lesson) e).getSubject()));
                int c = ((Lesson) e).getPeriod() == 0 ? weekId : week2Id;
                group.check((c));
                name.setKeyListener(null);
                name.setFocusable(false);
                name.setClickable(false);
                discript.setFocusable(false);
                discript.setClickable(false);
                discript.setKeyListener(null);
            } else {
                type = EVENT;
                spinner.setSelection(0);
            }
        } else {
            Calendar c = (Calendar) intent.getSerializableExtra("time");
            currentBegin = (Calendar) c.clone();
            currentEnd = (Calendar) c.clone();
            currentEnd.add(Calendar.HOUR_OF_DAY, 1);
        }
    }

    private void colorSetInit() {
        colorSet = new int[]{
                android.R.color.holo_blue_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_purple
        };
        currentColor = 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.time_edit_start:
                TimePickerDialog dialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                currentBegin.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                currentBegin.set(Calendar.MINUTE, minute);
                                setTimeAsText();
                            }
                        }, currentBegin.get(Calendar.HOUR_OF_DAY), currentBegin.get(Calendar.MINUTE), true);
                dialog.show();
                break;
            case R.id.time_edit_end:
                TimePickerDialog dialog1 = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                currentEnd.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                currentEnd.set(Calendar.MINUTE, minute);
                                setTimeAsText();
                            }
                        }, currentEnd.get(Calendar.HOUR_OF_DAY), currentEnd.get(Calendar.MINUTE), true);
                dialog1.show();
                break;
            case R.id.create_new_event_button:
                if (!isRightForm()) {
                    Toast.makeText(this, "Неверная форма заполнения", Toast.LENGTH_LONG).show();
                    return;
                }

                Intent i = new Intent(this, ScheduleActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Event xp = getIntent().getParcelableExtra(PUT_EVENT);
                if (xp != null) {
                    if (xp instanceof Lesson) {
                        db.updateLesson((Lesson) update(xp));
                    } else {
                        db.updateEvent(update(xp));
                    }
                } else {
                    createNewEvent();
                }
                startActivity(i);
                break;
            case R.id.color_button:
                colorButton.setBackgroundColor(getResources().getColor(colorSet[++currentColor % colorSet.length]));
                break;
            case R.id.cancel_new_event_button:
                super.onBackPressed();
                break;
        }
    }


    private Event update(Event event) {
        event.setName(name.getText().toString());
        event.setColorId(colorSet[currentColor % colorSet.length]);
        event.setDateStart(currentBegin);
        event.setDateEnd(currentEnd);
        event.setDiscript(discript.getText().toString());
        if (event instanceof Lesson) {
            ((Lesson) event).setSubject(subject);
            ((Lesson) event).setPeriod(isWeekPeriod);
        }
        return event;
    }


    private boolean isRightForm() {
        boolean fields = !name.getText().equals("");
        boolean lessons = (type == LESSON && (subjectsSpinner.getSelectedItem() != null)) || (type == EVENT);
        boolean periodcheck = ((group.getCheckedRadioButtonId() != 0) && (type == LESSON)) || (type == EVENT);
        return fields && lessons && periodcheck;
    }

    private void setTimeAsText() {
        timeToView(currentBegin, begin);
        timeToView(currentEnd, end);
    }

    private void timeToView(Calendar currentBegin, Button begin) {
        String hour;
        if (currentBegin.get(Calendar.HOUR_OF_DAY) < 10) {
            hour = "0" + currentBegin.get(Calendar.HOUR_OF_DAY);
        } else {
            hour = currentBegin.get(Calendar.HOUR_OF_DAY) + "";
        }
        String min;
        if (currentBegin.get(Calendar.MINUTE) < 10) {
            min = "0" + currentBegin.get(Calendar.MINUTE);
        } else {
            min = currentBegin.get(Calendar.MINUTE) + "";
        }
        begin.setText(hour + ":" + min);
    }

    private void createNewEvent() {
        if (type == EVENT) {
            db.addEvent(new Event(generateId(), name.getText().toString(),
                    discript.getText().toString(),
                    currentBegin, currentEnd, colorSet[currentColor % colorSet.length]));
        } else if (type == LESSON) {
            db.addEvent(new Lesson(generateId(), name.getText().toString(),
                    discript.getText().toString(),
                    currentBegin,
                    currentEnd,
                    subject, isWeekPeriod, colorSet[currentColor % colorSet.length]));
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.week_1:
                isWeekPeriod = EVERY_WEEK;
                break;
            case R.id.week_2:
                isWeekPeriod = EVERY_TWO_WEEK_COUNT;
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                type = EVENT;
                group.getChildAt(0).setEnabled(false);
                group.getChildAt(1).setEnabled(false);
                subjectsSpinner.setEnabled(false);
                subject = null;
                if (getIntent().getSerializableExtra("event") == null) {
                    name.getText().clear();
                    discript.getText().clear();
                }
                name.setFocusableInTouchMode(true);
                name.setClickable(true);
                discript.setFocusableInTouchMode(true);
                discript.setClickable(true);
                break;
            case 1:
                type = LESSON;
                group.getChildAt(0).setEnabled(true);
                group.getChildAt(1).setEnabled(true);
                subjectsSpinner.setEnabled(true);
                subjectsSpinner.setSelection(0);
                if (array.size() != 0) {
                    subject = array.get(0);
                    name.setText(subject.getName());
                    discript.setText(subject.getClassNum());
                }
                name.setFocusable(false);
                name.setClickable(false);
                discript.setFocusable(false);
                discript.setClickable(false);
                group.check(weekId);
                isWeekPeriod = EVERY_WEEK;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private long generateId() {
        return System.currentTimeMillis();
    }
}
