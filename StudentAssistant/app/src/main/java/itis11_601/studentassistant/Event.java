package itis11_601.studentassistant;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.Note.NoteHandler;

public class Event extends NoteHandler implements Parcelable, Serializable{

    private String name;
    private String discript;
    private Calendar dateStart;
    private Calendar dateEnd;
    private Context context;
    private int colorId;
    private long id;

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Event(long id, ArrayList<Note> notes, String name, String discript, Calendar date, Calendar dateEnds, int colorId) {
        super(notes);
        this.name = name;
        this.discript = discript;
        this.dateStart = date;
        this.dateEnd = dateEnds;
        this.colorId = colorId;
        this.id = id;
    }
    public Event(long id, String name, String discript, Calendar date, Calendar dateEnds, int colorId) {
        this(id, null, name, discript, date, dateEnds, colorId);
    }

    public Event(long id, String name, String discript, Calendar date, Calendar dateEnds) {
        this(id, null, name, discript, date, dateEnds, android.R.color.holo_blue_dark);
    }

    public Event(String name, String discript, Calendar date, Calendar dateEnd) {
        this(0, name, discript, date, dateEnd, android.R.color.holo_blue_dark);
    }

    public Event(ArrayList<Note> notes, String name, String discript, Calendar dateStart, Calendar dateEnd) {
        super(notes);
        this.name = name;
        this.discript = discript;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }

//    protected Event(Parcel in) {
//        super(new ArrayList<Note>());
//        name = in.readString();
//        discript = in.readString();
//        dateStart = (Calendar) in.readSerializable();
//        dateEnd = (Calendar) in.readSerializable();
//        colorId = in.readInt();
//        setNotes(in.readArrayList(null));
//    }


    public void updateNote(Note lastNote, Note newNote) {
        for (int i = 0; i < notes.size(); i++) {
            Log.d("SonyaNote", "in event has: " + notes.get(i).getTitle());
        }
        if (getNotes() != null) {
            int index = notes.indexOf(lastNote);
            Log.d("SonyaNote","searched position: " + index);
            getNotes().set(index, newNote);
        }

        //DBHelper dbHelper = DBHelper.getDbHelper(context);
        //dbHelper.updateNote(this, lastNote, newNote);
    }

//    public static final Creator<Event> CREATOR = new Creator<Event>() {
//        @Override
//        public Event createFromParcel(Parcel in) {
//            return new Event(in);
//        }
//
//        @Override
//        public Event[] newArray(int size) {
//            return new Event[size];
//        }
//    };

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext(){
        return context;
    }

    public void addNote(Note note) {
        getNotes().add(note);
        if (id == 0) id = System.currentTimeMillis();

        DBHelper dbHelper = DBHelper.getDbHelper(context);
        dbHelper.addNote(this, note);
    }

    public void removeNote(Note note) {
        getNotes().remove(note);

        DBHelper dbHelper = DBHelper.getDbHelper(context);
        dbHelper.removeNote(this, note);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscript() {
        return discript;
    }

    public void setDiscript(String discript) {
        this.discript = discript;
    }


    public Calendar getDateStart() {
        return dateStart;
    }

    public void setDateStart(Calendar date) {
        this.dateStart = date;
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(name);
//        dest.writeString(discript);
//        dest.writeSerializable(dateStart);
//        dest.writeSerializable(dateEnd);
//        dest.writeInt(colorId);
//        dest.writeParcelableArray(getNotes());
//    }

    public Calendar getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Calendar dateEnd) {
        this.dateEnd = dateEnd;
    }

    protected Event(Parcel in) {
        name = in.readString();
        discript = in.readString();
        dateStart = (Calendar) in.readValue(Calendar.class.getClassLoader());
        dateEnd = (Calendar) in.readValue(Calendar.class.getClassLoader());
        colorId = in.readInt();
        id = in.readLong();
        if (in.readByte() == 0x01) {
            notes = new ArrayList<Note>();
            in.readList(notes, Note.class.getClassLoader());
        } else {
            notes = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(discript);
        dest.writeValue(dateStart);
        dest.writeValue(dateEnd);
        dest.writeInt(colorId);
        dest.writeLong(id);
        if (notes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(notes);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
