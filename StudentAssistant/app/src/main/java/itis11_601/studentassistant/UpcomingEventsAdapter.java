package itis11_601.studentassistant;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.timetable.EventActivity;

/**
 * Created by пользователь on 14.04.2017.
 */

public class UpcomingEventsAdapter extends RecyclerView.Adapter<UpcomingEventsAdapter.ViewHolder>{


    ArrayList<Event> events;
    ArrayList<Note> notes;
    Activity context;


    public UpcomingEventsAdapter(ArrayList<Event> events) {
        this.events = events;
        notes = new ArrayList<>();
    }

    public UpcomingEventsAdapter(Activity context, ArrayList<Event> events) {
        this.events = events;
        notes = new ArrayList<>();
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_events_card, parent, false);
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d("SonyaEvent", "size " + events.size() + " position  " + position);
        Event event = events.get(position);
        if (event.getNotes() != null)
            notes = event.getNotes();
        else
            notes = new ArrayList<>();
        holder.name.setText(event.getName());
        holder.descript.setText(event.getDiscript());
        holder.date.setText(new SimpleDateFormat("dd.MM.yyyy").format(event.getDateStart().getTime()));
        holder.setId(position);
        UpcomingEventsNoteAdapter upcomingEventsNoteAdapter = new UpcomingEventsNoteAdapter(notes, context, event);
        upcomingEventsNoteAdapter.setId(position);
        holder.notes.setAdapter(upcomingEventsNoteAdapter);
        holder.event = event;
        Log.d("SonyaUp", position + "    " + event.getName());
    }
    @Override
    public int getItemCount() {
        return events.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView descript;
        private RecyclerView notes;
        private TextView date;
        private CardView cv;
        private Event event;
        private long id;
        private ImageButton ib;

        public ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card);
            name = (TextView) itemView.findViewById(R.id.title);
            descript = (TextView) itemView.findViewById(R.id.descript);
            date = (TextView) itemView.findViewById(R.id.date);
            notes = (RecyclerView) itemView.findViewById(R.id.rv_for_notes);
            ib = (ImageButton) itemView.findViewById(R.id.imageButton);
            ib.setFocusable(false);
            ib.setId((int) id);

            LinearLayoutManager llm = new LinearLayoutManager(context);
            notes.setLayoutManager(llm);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, EventActivity.class);
                    Log.d("SonyaEvent", "id click" + id);
                    i.putExtra("event", (Serializable) event);
                    context.startActivity(i);
                }
            });

        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
            ib.setId((int) id);
        }
    }
}
