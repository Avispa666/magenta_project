package itis11_601.studentassistant.NotificationService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Максим on 07.04.2017.
 */
public class DeferredNotification extends BroadcastReceiver {

    public static final String TAG = "NOTTEST";

    public static final String
            TITLE = "TITLE",
            SUBJECT = "SUbJEcT",
            ACTIVITY_NAME = "NAME_OF_ACTIVITY",
            ICON_RES_ID = "IconniRESSdsID";


    public void onReceive(Context context, Intent prevIntent) {

        Log.d("SonyaNotiS", "yeeeeeeeee");
        Class<? extends Context> clazz = null;
        Bundle ex = prevIntent.getExtras();
        try {
            clazz = (Class<? extends Context>) Class.forName(ex.getString(ACTIVITY_NAME));
        } catch (ClassCastException | ClassNotFoundException e) {
            throw new RuntimeException("Wrong ClassName = " + ex.getString(ACTIVITY_NAME));
        }
        Intent intent = new Intent(context, clazz);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        // build notification
        // the addAction re-use the same intent to keep the example short
        int icon = ex.getInt(ICON_RES_ID,0);
        //Notification n = new NotificationCompat.Builder(context)
        Notification n  = new Notification.Builder(context)
                .setContentTitle(ex.getString(TITLE))
                .setContentText(ex.getString(SUBJECT))
                .setSmallIcon(android.R.mipmap.sym_def_app_icon)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL)
                //.setCategory(Notification.CATEGORY_STATUS)
                //.setShowWhen(true)
                .setWhen(currentTimeMillis())
                .addAction(icon, "And more", pIntent)
                .build();


        //NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);
    }
}