package itis11_601.studentassistant.NotificationService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.util.Log;

import java.util.Date;

import static itis11_601.studentassistant.NotificationService.DeferredNotification.ACTIVITY_NAME;
import static itis11_601.studentassistant.NotificationService.DeferredNotification.ICON_RES_ID;
import static itis11_601.studentassistant.NotificationService.DeferredNotification.SUBJECT;
import static itis11_601.studentassistant.NotificationService.DeferredNotification.TITLE;

/**
 * Created by Максим on 11.04.2017.
 */
public class DeferredNotificationBuilder {
    private Context context;
    private int id;
    private Class activityClass;
    private String title, subject;
    private
    @DrawableRes
    int iconResId;
    private long timeInMillis;
    private boolean created = false;

    public DeferredNotificationBuilder(
            Context context,
            Class activityClass,
            String title,
            String subject,
            int iconResId,
            long timeInMillis,
            int id) {

        this.context = context;
        this.activityClass = activityClass;
        this.title = title;
        this.subject = subject;
        this.iconResId = iconResId;
        this.timeInMillis = timeInMillis;
        this.id = id;
    }

    private void setId(int id) {
        this.id = id;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Class getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(Class activityClass) {
        this.activityClass = activityClass;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    public void setTimeInMillis(long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }

    public static void deleteById(Context context, int id) {
        Intent intent = new Intent(context, DeferredNotification.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    public void create() {
        Log.d("SonyaNotiS", "i create am");
        if (created)
            throw new RuntimeException("Notification : \n " + toString() + "\n already created");
        created = true;

        Intent intent = new Intent(context, DeferredNotification.class);
        Bundle bundle = new Bundle();
        bundle.putString(ACTIVITY_NAME, activityClass.getName());
        bundle.putString(SUBJECT, subject);
        bundle.putString(TITLE, title);
        bundle.putInt(ICON_RES_ID, iconResId);
        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
    }

    @Override
    public String toString() {
        return
                "[NOTIFICATION] title=" + title + "\n" +
                        "subject=" + subject + "\n" +
                        "to activity " + activityClass.getName() + "\n" +
                        "in time=" + (new Date(timeInMillis)) + "\n" +
                        "id = " + id;
    }

    public int getId() {
        return id;
    }
}
