package itis11_601.studentassistant;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import itis11_601.studentassistant.Statistic.SleepStats.SleepStatsHelper;

import static itis11_601.studentassistant.R.id.date;
import static itis11_601.studentassistant.R.id.thursday;
import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.Subject.Subject;

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_NAME_NOTES = "Notes";
    public static final String TABLE_NAME_SUBJECT = "Subject";
    public static final String TABLE_NAME_EVENT = "Event";
    public static final String TABLE_NAME_LESSON = "Lesson";
    public static final String TABLE_NAME_DAY_OF_WEEK = "Days";
    public static final String DB_NAME = "StudentAssistant";
    public static final String DAY_OF_WEEK = "DayWeek";
    public static final String DAY = "DayId";
    public static final String SUBJECT = "Subject";
    public static final String SUBJECT_ID = "SubId";
    public static final String TEACHER = "Teacher";
    public static final String CLASS_NUM = "ClassNum";
    public static final String NAME = "Name";
    public static final String DISCRIPT = "Discript";
    public static final String DATE = "Date";
    public static final String DATE_START = "DateStart";
    public static final String DATE_END = "DateEnd";
    public static final String PERIOD = "Period";
    public static final String EVENT_ID = "EvId";
    public static final String LESSON_ID = "LessId";
    public static final String KEY_ID = "_id";
    private static final String TABLE_NAME_SLEEP_STATS = "SleepStats";
    private static final String HOURS = "Hours";
    private static final String TAG = "DBHelper";
    public static final String KEY = "id";
    public static final String COLOR_ID = "ColorId";
    public static final String DATE_REMEMBER = "Remember";
    public static DBHelper dbHelper;
    public static final String MyLog = "MyLog";
    private static final int DATABASE_VERSION = 3;
    private SimpleDateFormat noteFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    private SimpleDateFormat lessonFormat = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat eventFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

//    public static synchronized DBHelper getDbHelper(Context context) {
//        if (dbHelper == null)
//            dbHelper = new DBHelper(context.getApplicationContext());
//        return dbHelper;
//    }

//    public static synchronized void closeDbHelper() {
//
//        if (dbHelper != null)
//            dbHelper.close();
//        dbHelper = null;
//    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME_SUBJECT + " ("
                + KEY_ID + " integer primary key autoincrement,"
                + SUBJECT + " text, "
                + TEACHER + " text, "
                + CLASS_NUM + " text);");
        db.execSQL("create table " + TABLE_NAME_DAY_OF_WEEK + " ("
                + KEY_ID + " integer primary key autoincrement,"
                + DAY_OF_WEEK + " text);");
        db.execSQL("create table " + TABLE_NAME_NOTES + " ("
                + KEY_ID + " integer primary key autoincrement,"
                + NAME + " text,"
                + DISCRIPT + " text,"
                + EVENT_ID + " integer,"
                + SUBJECT_ID + " integer, "
                + LESSON_ID + " integer, "
                + DATE_REMEMBER + " text, "
                + DATE + " text);");
        db.execSQL("create table " + TABLE_NAME_LESSON + " ("
                + KEY + " integer primary key, "
                + DAY + " integer, "
                + SUBJECT_ID + " integer, "
                + PERIOD + " integer, "
                + DISCRIPT + " text, "
                + COLOR_ID + " integer, "
                + DATE_START + " text, "
                + DATE_END + " text);");
        db.execSQL("create table " + TABLE_NAME_EVENT + " ("
                + KEY + " integer primary key,"
                + NAME + " text, "
                + DISCRIPT + " text, "
                + DATE_START + " text, "
                + COLOR_ID + " integer, "
                + DATE_END + " text);");
        db.execSQL("create table " + TABLE_NAME_SLEEP_STATS + " ("
                + KEY_ID + " integer primary key autoincrement,"
                + DATE + " text, "
                + HOURS + " integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < DATABASE_VERSION) {
            db.execSQL("ALTER TABLE " + TABLE_NAME_EVENT + " ADD COLUMN " + COLOR_ID + " integer;");
            db.execSQL("ALTER TABLE " + TABLE_NAME_LESSON + " ADD COLUMN " + COLOR_ID + " integer;");
        }
    }

    public static synchronized DBHelper getDbHelper(Context context) {
        if (dbHelper == null)
            dbHelper = new DBHelper(context);
        return dbHelper;
    }


    public static synchronized void closeDbHelper(){

        if(dbHelper!=null )
            dbHelper.close();
        dbHelper = null;
    }

    public ArrayList<Event> getAllEventsFromDB(Calendar dateFrom, Calendar dateTo) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Event> events = new ArrayList<>();

        Cursor c = db.query(TABLE_NAME_EVENT, null, DATE_START + ">=? and " + DATE_START + "<?", new String[]{eventFormat.format(dateFrom.getTime()), eventFormat.format(dateTo.getTime())}, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0) {
            do {
                String name = c.getString(c.getColumnIndex(NAME));
                String dis = c.getString(c.getColumnIndex(DISCRIPT));
                Calendar start = new GregorianCalendar();
                Calendar end = new GregorianCalendar();
                int color = c.getInt(c.getColumnIndex(COLOR_ID));
                try {
                    start.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_START))));
                    end.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_END))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Event event = new Event(name, dis, start, end);

                event.setId(c.getLong(c.getColumnIndex(KEY)));

                event.setColorId(color);
                event.setNotes(getNote(event));
                events.add(event);
            }while(c.moveToNext());
        }
        String table = TABLE_NAME_LESSON + " f INNER JOIN "+TABLE_NAME_SUBJECT + " i ON f." + SUBJECT_ID + " = i._id ";

        c = db.query(table, null, DAY + "= ?", new String[]{String.valueOf(dateFrom.DAY_OF_WEEK)}, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0) {
            do {
                String dis = c.getString(c.getColumnIndex(DISCRIPT));
                String teacher = c.getString(c.getColumnIndex(TEACHER));
                String sub = c.getString(c.getColumnIndex(SUBJECT));
                String clas = c.getString(c.getColumnIndex(CLASS_NUM));
                int color = c.getInt(c.getColumnIndex(COLOR_ID));
                Calendar start = new GregorianCalendar();
                Calendar end = new GregorianCalendar();
                try {
                    start.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_START))));
                    end.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_END))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int period = (int) c.getLong(c.getColumnIndex(PERIOD));
                //Log.d("MyLog", "i add lesson on all event " + teacher);
                Lesson lesson = new Lesson(sub, dis, start, end, new Subject(sub, teacher, clas), period);

                lesson.setId(c.getLong(c.getColumnIndex(KEY)));
                lesson.setColorId(color);
                lesson.setNotes(getNote(lesson));
                events.add(lesson);
            } while (c.moveToNext());
        }
        c.close();
        return events;
    }

    public ArrayList<Event> getAllEvents() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Event> events = new ArrayList<>();

        Cursor c = db.query(TABLE_NAME_EVENT, null, null, null, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0) {
            do {
                String name = c.getString(c.getColumnIndex(NAME));
                String dis = c.getString(c.getColumnIndex(DISCRIPT));
                int color = c.getInt(c.getColumnIndex(COLOR_ID));
                Calendar start = new GregorianCalendar();
                Calendar end = new GregorianCalendar();
                try {
                    start.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_START))));
                    end.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_END))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Event event = new Event(name, dis, start, end);

                event.setId(c.getLong(c.getColumnIndex(KEY)));
                Log.d("SonyaDB", "Event color " + color + "   " + event.getId());
                event.setColorId(color);
                event.setNotes(getNote(event));
                events.add(event);
                Log.d("SonyaDB", "i put from bd " + name + "  " + event.getId());

            }while(c.moveToNext());
        }
        String table = TABLE_NAME_LESSON + " f INNER JOIN "+TABLE_NAME_SUBJECT + " i ON f." + SUBJECT_ID + " = i._id ";

//        c = db.query(table, null, DAY + "= ?", new String[]{String.valueOf(Calendar.DAY_OF_WEEK)}, null, null, null);
        c = db.query(table, null, null, null, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0) {
            do {
                String dis = c.getString(c.getColumnIndex(DISCRIPT));
                String teacher = c.getString(c.getColumnIndex(TEACHER));
                String sub = c.getString(c.getColumnIndex(SUBJECT));
                String clas = c.getString(c.getColumnIndex(CLASS_NUM));
                int period = (int) c.getLong(c.getColumnIndex(PERIOD));
                int color = c.getInt(c.getColumnIndex(COLOR_ID));
                Calendar start = new GregorianCalendar();
                Calendar end = new GregorianCalendar();
                try {
                    start.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_START))));
                    end.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_END))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //Log.d("MyLog", "i add lesson on all event " + teacher);
                Lesson lesson = new Lesson(sub, dis, start, end, new Subject(sub, teacher, clas), period);
                Log.d("SonyaLesson", "i'm her");
                lesson.setId(c.getLong(c.getColumnIndex(KEY)));
                Log.d("SonyaLesson", "i put id  " + lesson.getId());
                lesson.setColorId(color);
                lesson.setNotes(getNote(lesson));
                events.add(lesson);

            } while (c.moveToNext());
        }
        c.close();
        return events;
    }

    public void addEvent(Event event) {
        Log.d(MyLog, "I start add event");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY, event.getId());
        cv.put(NAME, event.getName());
        cv.put(DISCRIPT, event.getDiscript());
        cv.put(DATE_START, eventFormat.format(event.getDateStart().getTime()));
        cv.put(DATE_END, eventFormat.format(event.getDateEnd().getTime()));
        cv.put(COLOR_ID, event.getColorId());
        db.insert(TABLE_NAME_EVENT, null, cv);
        Log.d(MyLog, "i add " + event.getName() + "  " + event.getDiscript() + "  " + eventFormat.format(event.getDateStart().getTime()));

    }

    public void addEvent(Lesson lesson) {
        //Log.d(MyLog, "I start add lesson");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        long pos = getSubjectId(lesson.getSubject());
        cv.put(KEY, lesson.getId());
        cv.put(DAY, String.valueOf(lesson.getDateStart().DAY_OF_WEEK));
        cv.put(SUBJECT_ID, pos);
        cv.put(DATE_START, eventFormat.format(lesson.getDateStart().getTime()));
        cv.put(DATE_END, eventFormat.format(lesson.getDateEnd().getTime()));
        cv.put(PERIOD, lesson.getPeriod());
        cv.put(COLOR_ID, lesson.getColorId());
        Log.d("SonyaLesson", "i add lesson with " + lesson.getId() + "  " + lesson.getName());
        db.insert(TABLE_NAME_LESSON, null, cv);
        //Log.d(MyLog, "i add " + String.valueOf(lesson.getDate().DAY_OF_WEEK) + " "+ lesson.getName() + "  " + pos + " " + lesson.getSubject().getTeacher() + "  " + lessonFormat.format(lesson.getDate().getTime()));
    }

    public void addSubject(Subject subject) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv2 = new ContentValues();
        cv2.put(TEACHER, subject.getTeacher());
        cv2.put(CLASS_NUM, subject.getClassNum());
        cv2.put(SUBJECT, subject.getName());
        db.insert(TABLE_NAME_SUBJECT, null, cv2);
    }

    public long getLessonId(Lesson event) {
        SQLiteDatabase db = this.getReadableDatabase();
//        String id = "" + getSubjectId(event.getSubject());
//        String day = String.valueOf(Calendar.DAY_OF_WEEK);
//        Cursor c = db.query(TABLE_NAME_LESSON, null, DAY + "=? and " + PERIOD + "=? and " + SUBJECT_ID + "=? and " + DATE +"=?",
//                new String[]{day, String.valueOf(event.getPeriod()), id, lessonFormat.format(event.getDate().getTime())},
//                        null, null, null);
        String id = String.valueOf(event.getId());
        String day = String.valueOf(event.getDateStart().DAY_OF_WEEK);

        Cursor c = db.query(TABLE_NAME_LESSON, null, KEY + "=?",
                new String[]{id},
                      null, null, null);
        long pos = 0;
        if (c.moveToFirst() && c.getCount() != 0) {
            pos = c.getLong(c.getColumnIndex(KEY));
        }


        else {
            Log.d("SonyaDB", "OOPPS in lesson");
           // pos = System.currentTimeMillis();
            pos = event.getId();
            ContentValues cv2 = new ContentValues();
            cv2.put(KEY, event.getId());
            cv2.put(SUBJECT_ID, id);
            cv2.put(DAY, day);
            cv2.put(DATE_START, eventFormat.format(event.getDateStart().getTime()));
            cv2.put(DATE_END, eventFormat.format(event.getDateEnd().getTime()));
            cv2.put(PERIOD, event.getPeriod());
            cv2.put(COLOR_ID, event.getColorId());
            db.insert(TABLE_NAME_LESSON, null, cv2);
        }
        Log.d("SonyaLesson", "i try " + event.getId() + "   " + pos);
        c.close();
        return pos;
    }

    public double getAverageHoursLastWeek() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        int sum = 0;
        int count = 0;
        if (c.moveToLast()) {
            do {
                sum += c.getInt(c.getColumnIndex(HOURS));
                ++count;
            } while (count < 7 && c.moveToPrevious());
        }
        Log.d(TAG, "getAverageHoursLastWeek: " + count);
        c.close();
        if (count == 0) return 0;
        return (double) sum / count;
    }

    public double getAverageHoursLastMonth() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        int sum = 0;
        int count = 0;
        if (c.moveToLast()) {
            do {
                sum += c.getInt(c.getColumnIndex(HOURS));
                ++count;
            } while (count < 30 && c.moveToPrevious());
        }
        Log.d(TAG, "getAverageHoursLastMonth: " + count);
        c.close();
        if (count == 0) return 0;
        return (double) sum / count;
    }

    public Date getLastRecordDate() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        if (c.moveToLast()) {
            String date = c.getString(c.getColumnIndex(DATE));
            return SleepStatsHelper.parseDate(date);
        }
        return null;
    }

    public void addHours(int hours) {
        Log.d(TAG, "addHours: " + hours);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        if (!SleepStatsHelper.compareDates(getLastRecordDate(), new Date()) || true) {
            cv.put(HOURS, hours);
            cv.put(DATE, SleepStatsHelper.formatDate(new Date()));
            db.insert(TABLE_NAME_SLEEP_STATS, null, cv);
        }
    }

    public void deleteLastHours(Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        if (c.getCount() != 0) {
            c.moveToLast();
            int id = c.getInt(c.getColumnIndex("_id"));
            Log.d("DBhelper", "deleteLastHours: id = " + id);
            db.delete(TABLE_NAME_SLEEP_STATS, "_id = " + id, null);
        } else {
            Toast.makeText(context, "Database is empty", Toast.LENGTH_SHORT).show();
        }
        c.close();
    }

    public long getSubjectId(Subject subject) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SUBJECT, null, TEACHER + "=? and " + CLASS_NUM + "=? and " + SUBJECT + "=?",
                new String[]{subject.getTeacher(), subject.getClassNum(), subject.getName()}, null, null, null);

        long pos = 0;
        if (c.moveToFirst() && c.getCount() != 0) {
            pos = c.getLong(c.getColumnIndex(KEY_ID));
        }

        else {
            Log.d("SonyaDB", "i create new subject");
            ContentValues cv2 = new ContentValues();
            cv2.put(TEACHER, subject.getTeacher());
            cv2.put(CLASS_NUM, subject.getClassNum());
            cv2.put(SUBJECT, subject.getName());
            pos = db.insert(TABLE_NAME_SUBJECT, null, cv2);
        }
        c.close();
        return pos;
    }

    public long getEventId(Event event) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_EVENT, null, KEY + "=?",
                new String[]{String.valueOf(event.getId())},
                null, null, null);
        long pos = 0;
        if (c.moveToFirst() && c.getCount() != 0) {
            pos = event.getId();
        }

        else {

            pos = event.getId();
            Log.d("SonyaDB", "OOOOPPPSSS, it's bad     " + pos + event.getName());
            ContentValues cv2 = new ContentValues();
            cv2.put(KEY, event.getId());
            cv2.put(NAME, event.getName());
            cv2.put(DISCRIPT, event.getDiscript());
            cv2.put(DATE_START, eventFormat.format(event.getDateStart().getTime()));
            cv2.put(DATE_END, eventFormat.format(event.getDateEnd().getTime()));
            cv2.put(COLOR_ID, event.getColorId());
            db.insert(TABLE_NAME_EVENT, null, cv2);
        }
        c.close();
        return pos;
    }

    public void addNote(Event event, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        long pos = getEventId(event);
        cv.put(EVENT_ID, pos);
        cv.put(NAME, note.getTitle());
        cv.put(DISCRIPT, note.getDiscript());
        cv.put(DATE, noteFormat.format(event.getDateStart().getTime()));

        db.insert(TABLE_NAME_NOTES, null, cv);
    }

    public void addNote(Lesson lesson, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        Log.d("SonyaLesson", "i try add note");
        long pos = getLessonId(lesson);
        cv.put(LESSON_ID, pos);
        cv.put(NAME, note.getTitle());
        cv.put(DATE, noteFormat.format(lesson.getDateStart().getTime()));
        cv.put(DISCRIPT, note.getDiscript());
        Log.d("SonyaDB", "i add new note " + note.getTitle());
        db.insert(TABLE_NAME_NOTES, null, cv);
    }

    public void addNote(Subject subject, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        if (note.getId() != 0) {
            cv.put(KEY_ID, note.getId());
        }
        cv.put(NAME, note.getTitle());
        cv.put(DISCRIPT, note.getDiscript());
        cv.put(SUBJECT_ID, getSubjectId(subject));
        db.insert(TABLE_NAME_NOTES, null, cv);
    }

    public ArrayList<Note> getNote(Lesson lesson) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Note> notes = new ArrayList<>();
        Log.d("SonyaLesson", "i try get note   " + lesson.getId());
        String pos = String.valueOf(getLessonId(lesson));
        Cursor c = db.query(TABLE_NAME_NOTES, null, LESSON_ID + "=? and " + DATE + "=?", new String[]{pos, noteFormat.format(lesson.getDateStart().getTime())}, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0){
            do{
                String title = c.getString(c.getColumnIndex(NAME));
                String dis = c.getString(c.getColumnIndex(DISCRIPT));
                notes.add(new Note(title, dis, lesson));
            }while (c.moveToNext());
        }
        c.close();
        return notes;
    }

    public ArrayList<Note> getNote(Event event) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Note> notes = new ArrayList<>();

        String pos = String.valueOf(getEventId(event));
        Log.d("SonyaDB", "i get note " + event.getId() + "   " + pos + "  " + event.getName());
        Cursor c = db.query(TABLE_NAME_NOTES, null, EVENT_ID + "=?", new String[]{pos}, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0){
            do{
                String title = c.getString(c.getColumnIndex(NAME));
                String dis = c.getString(c.getColumnIndex(DISCRIPT));
                notes.add(new Note(title, dis, event));
            }while (c.moveToNext());
        }
        c.close();
        return notes;
    }

    public ArrayList<Note> getActualNote(Subject subject, Calendar date) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Note> notes = new ArrayList<>();
        ArrayList<Lesson> lessons = new ArrayList<>();

        String pos = String.valueOf(getSubjectId(subject));
        Cursor c;

        c = db.query(TABLE_NAME_LESSON, null, SUBJECT_ID + "=?", new String[]{"" + pos}, null, null, null);
        ArrayList<String> buf = new ArrayList<>();
        if (c.moveToFirst() && c.getCount() != 0) {
            do {
                long k = c.getLong(c.getColumnIndex(KEY));
                String disc = c.getString(c.getColumnIndex(DISCRIPT));
                Calendar dateS = new GregorianCalendar();
                try {
                    dateS.setTime(eventFormat.parse(c.getString(c.getColumnIndex(DATE_START))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                lessons.add(new Lesson(subject.getName(), disc, dateS, null, subject, 0));
                buf.add("" + k );
            }while (c.moveToNext());
        }
        for (int i = 0; i < buf.size(); i++) {
            Lesson lesson = lessons.get(i);
            c = db.query(TABLE_NAME_NOTES, null, LESSON_ID + "=? and " + DATE  + ">=?", new String[] {buf.get(i), noteFormat.format(date.getTime())}, null, null, DATE + " ASC");
            if (c.moveToFirst() && c.getCount() != 0) {
                do {
                    String title = c.getString(c.getColumnIndex(NAME));
                    String dis = c.getString(c.getColumnIndex(DISCRIPT));
                    Calendar dateNote = new GregorianCalendar();
                    try {
                        dateNote.setTime(noteFormat.parse(c.getString(c.getColumnIndex(DATE))));
                    } catch (ParseException e) {
                        Log.d("SonyaDB", "Oops, can't parse string in date");
                        //e.printStackTrace();
                    }
                    lesson.setDateStart(dateNote);
                    notes.add(new Note(title, dis, lesson));
                } while (c.moveToNext());
            }
        }
        c = db.query(TABLE_NAME_NOTES, null, SUBJECT_ID + "=?", new String[]{"" + pos}, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0){
            do{
                String title = c.getString(c.getColumnIndex(NAME));
                String dis = c.getString(c.getColumnIndex(DISCRIPT));
                notes.add(new Note(title, dis, subject));
            }while (c.moveToNext());
        }
        c.close();
        return notes;
    }

    public ArrayList<Note> getArchiveNote(Subject subject, Calendar date) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Note> notes = new ArrayList<>();
        ArrayList<Lesson> lessons = new ArrayList<>();

        String pos = String.valueOf(getSubjectId(subject));
        Cursor c;

        c = db.query(TABLE_NAME_LESSON, null, SUBJECT_ID + "=?", new String[]{"" + pos}, null, null, null);
        ArrayList<String> buf = new ArrayList<>();
        if (c.moveToFirst() && c.getCount() != 0) {
            do {
                long k = c.getLong(c.getColumnIndex(KEY));
                String disc = c.getString(c.getColumnIndex(DISCRIPT));
                Calendar dateS = new GregorianCalendar();
                try {
                    dateS.setTime(noteFormat.parse(c.getString(c.getColumnIndex(DATE_START))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                lessons.add(new Lesson(subject.getName(), disc, null, null, subject, 0));
                buf.add("" + k );
            }while (c.moveToNext());
        }
        for (int i = 0; i < buf.size(); i++) {
            Lesson lesson = lessons.get(i);
            c = db.query(TABLE_NAME_NOTES, null, LESSON_ID + "=? and " + DATE  + "<?", new String[] {buf.get(i), noteFormat.format(date.getTime())}, null, null, DATE + " ASC");
            if (c.moveToFirst() && c.getCount() != 0) {
                do {
                    String title = c.getString(c.getColumnIndex(NAME));
                    String dis = c.getString(c.getColumnIndex(DISCRIPT));
                    Calendar dateNote = new GregorianCalendar();
                    try {
                        dateNote.setTime(noteFormat.parse(c.getString(c.getColumnIndex(DATE))));
                    } catch (ParseException e) {
                        Log.d("SonyaDB", "Oops, can't parse string in date");
                        //e.printStackTrace();
                    }
                    lesson.setDateStart(dateNote);
                    notes.add(new Note(title, dis, lesson));
                } while (c.moveToNext());
            }
        }
        return notes;
    }

    public void removeNote(Event event, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_NOTES, NAME + "=? and "
                        + DISCRIPT + "=? and "
                        + EVENT_ID + "=?", new String[] {note.getTitle(), note.getDiscript(), String.valueOf(getEventId(event))});
    }

    public void removeNote(Lesson lesson, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("SonyaDB", "i remove note from db " + note.getTitle());
        db.delete(TABLE_NAME_NOTES, NAME + "=? and "
                + DISCRIPT + "=? and "
                + LESSON_ID + "=?", new String[]{note.getTitle(), note.getDiscript(), String.valueOf(getLessonId(lesson))});
    }

    public void removeNote(Subject subject, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_NOTES, NAME + "=? and "
                + DISCRIPT + "=? and "
                + SUBJECT_ID + "=?", new String[] {note.getTitle(), note.getDiscript(), String.valueOf(getSubjectId(subject))});
    }

    public void removeEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();

        long pos = getEventId(event);
        db.delete(TABLE_NAME_NOTES, EVENT_ID + "=" + pos, null);
        db.delete(TABLE_NAME_EVENT, KEY + "=" + pos, null);
        Log.d("SonyaDB", "i delete " + pos);
    }

    public void removeEvent(Lesson lesson) {
        SQLiteDatabase db = this.getWritableDatabase();

        long pos = getLessonId(lesson);
        Log.d("SonyaLesson", "i delete " + pos);
        db.delete(TABLE_NAME_NOTES, LESSON_ID + "=" + pos, null);
        db.delete(TABLE_NAME_LESSON, KEY + "=" + pos, null);
    }

    public void removeSubject(Subject subject) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("SonyaBD", "i delete subject with name " + subject.getName());

        long pos = getSubjectId(subject);
        db.delete(TABLE_NAME_NOTES, SUBJECT_ID + "=" + pos, null);
        Cursor c = db.query(TABLE_NAME_LESSON, null, SUBJECT_ID + "=" + pos, null, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0) {
            do {
                String buf = c.getString(c.getColumnIndex(KEY));
                db.delete(TABLE_NAME_NOTES, LESSON_ID + "=" + buf, null);
            } while (c.moveToNext());
        }
        db.delete(TABLE_NAME_LESSON, SUBJECT_ID + "=" + pos, null);
        db.delete(TABLE_NAME_SUBJECT, KEY_ID + "=" + pos, null);
    }

    public ArrayList<Subject> getAllSubjects(){
        Log.d("DB Log", "I try get");
        SQLiteDatabase sub = this.getReadableDatabase();
        ArrayList<Subject> lessons = new ArrayList<>();
        Cursor c = sub.query(TABLE_NAME_SUBJECT, null, null, null, null, null, null);
        if(c.moveToFirst()){
            do {
                String subject = c.getString(c.getColumnIndex(SUBJECT));
                String teacher = c.getString(c.getColumnIndex(TEACHER));
                String clas = c.getString(c.getColumnIndex(CLASS_NUM));
                lessons.add(new Subject(subject, teacher, clas));
            } while (c.moveToNext());
        }
        c.close();
        return lessons;
    }

    public int getLastDayHours() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        if (c.moveToLast()) {
            int t = c.getInt(c.getColumnIndex(HOURS));
            if (SleepStatsHelper.compareDates(getLastRecordDate(), new Date())) {
                c.close();
                return t;
            }
        }
        c.close();
        return 0;
    }

    public int getMinHoursLastWeek() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        int min = 24;
        int count = 0;
        if (c.moveToLast()) {
            do {
                int t = c.getInt(c.getColumnIndex(HOURS));
                if (t < min) min = t;
                ++count;
            } while (count < 7 && c.moveToPrevious());
        }
        c.close();
        if (count == 0) return 0;
        return min;
    }

    public int getMinHoursLastMonth() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        int min = 24;
        int count = 0;
        if (c.moveToLast()) {
            do {
                int t = c.getInt(c.getColumnIndex(HOURS));
                if (t < min) min = t;
                ++count;
            } while (count <= 30 && c.moveToPrevious());
        }
        c.close();
        if (count == 0) return 0;
        return min;
    }

    public int getMaxHoursLastWeek() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        int max = 0;
        int count = 0;
        if (c.moveToLast()) {
            do {
                int t = c.getInt(c.getColumnIndex(HOURS));
                if (t > max) max = t;
                ++count;
            } while (count < 7 && c.moveToPrevious());
        }
        c.close();
        if (count == 0) return 0;
        return max;
    }

    public int getMaxHoursLastMonth() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        int max = 0;
        int count = 0;
        if (c.moveToLast()) {
            do {
                int t = c.getInt(c.getColumnIndex(HOURS));
                if (t > max) max = t;
                ++count;
            } while (count <= 30 && c.moveToPrevious());
        }
        c.close();
        if (count == 0) return 0;
        return max;
    }
    public ArrayList<String> getAllHoursLastWeek() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SLEEP_STATS, null, null, null, null, null, null);
        ArrayList<String> res = new ArrayList<>(7);
        SimpleDateFormat format = new SimpleDateFormat("d MMMM");
        int sum = 0;
        int count = 0;
        if (c.moveToLast()) {
            do {
                int t = c.getInt(c.getColumnIndex(HOURS));
                String date = format.format(SleepStatsHelper.parseDate(c.getString(c.getColumnIndex(DATE))));
                res.add(date + ": " + t  + "ч.");
                ++count;
            } while (count < 7 && c.moveToPrevious());
        }
        c.close();
        Collections.reverse(res);
        return res;
    }

    public void updateSubject(Subject lastSubject, Subject newSubject) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SUBJECT, newSubject.getName());
        cv.put(TEACHER, newSubject.getTeacher());
        cv.put(CLASS_NUM, newSubject.getClassNum());

        long id = getSubjectId(lastSubject);

        db.update(TABLE_NAME_SUBJECT, cv, KEY_ID + " = " + String.valueOf(id),
                null);
    }

    public void updateNote(Event event, Note lastNote, Note newNote) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, newNote.getTitle());
        cv.put(DISCRIPT, newNote.getDiscript());

        String id = String.valueOf(getEventId(event));

        db.update(TABLE_NAME_NOTES,cv, NAME + "=? and "
                    + DISCRIPT + "=? and " + EVENT_ID + "=? and "
                    + DATE + "=?", new String[] { lastNote.getTitle(), lastNote.getDiscript(), id, noteFormat.format(event.getDateStart().getTime())});
    }

    public void updateEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(NAME, event.getName());
        cv.put(DISCRIPT, event.getDiscript());
        cv.put(DATE_START, eventFormat.format(event.getDateStart().getTime()));
        cv.put(DATE_END, eventFormat.format(event.getDateEnd().getTime()));
        cv.put(COLOR_ID, event.getColorId());

        db.update(TABLE_NAME_EVENT, cv, KEY + "=?", new String[] {String.valueOf(event.getId())});
    }

    public void updateLesson(Lesson event) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLOR_ID, event.getColorId());
        cv.put(DISCRIPT, event.getDiscript());
        cv.put(DATE_START, eventFormat.format(event.getDateStart().getTime()));
        cv.put(DATE_END, eventFormat.format(event.getDateEnd().getTime()));
        cv.put(SUBJECT_ID, getSubjectId(event.getSubject()));
        cv.put(PERIOD, event.getPeriod());

        db.update(TABLE_NAME_LESSON, cv, KEY + "=?", new String[] {String.valueOf(event.getId())});
    }

    public void updateNote(Subject subject, Note lastNote, Note newNote) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, newNote.getTitle());
        cv.put(DISCRIPT, newNote.getDiscript());

        long id = getSubjectId(subject);

        db.update(TABLE_NAME_NOTES,cv, NAME + " =? and "
                + DISCRIPT + "=? and "
                + SUBJECT_ID + "=?", new String[] {lastNote.getTitle(), lastNote.getDiscript(), String.valueOf(id)});
    }

    public void getAllNoteTest() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_NOTES, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                Log.d("SonyaDB", "titel " + c.getString(c.getColumnIndex(NAME)));
            }while (c.moveToNext());
        }
    }

    public void clearDB() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_NAME_LESSON, null, null);
        db.delete(TABLE_NAME_NOTES, null, null);
        db.delete(TABLE_NAME_EVENT, null, null);
        db.delete(TABLE_NAME_SUBJECT, null, null);
    }
}
