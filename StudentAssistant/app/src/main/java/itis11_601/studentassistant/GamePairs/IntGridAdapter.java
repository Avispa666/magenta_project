package itis11_601.studentassistant.GamePairs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import itis11_601.studentassistant.GamePairs.Plate;
import itis11_601.studentassistant.R;

/**
 * Created by 1 on 24.03.2017.
 */

public class IntGridAdapter extends BaseAdapter {

    private Context context;
    private Plate[] plates;
    private LayoutInflater inflater;

    public IntGridAdapter(Context context, Plate[] array) {
        this.context = context;
        this.plates = array;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return plates.length;
    }

    @Override
    public Plate getItem(int position) {
        return plates[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("AAAAAAAAAAAA", position + " is position");
        View v = convertView;
        if (v == null) {
            v = inflater.inflate(R.layout.game_pairs_item, parent, false);
        }
        Plate plate = getItem(position);
        ((TextView) v.findViewById(R.id.text_item)).setText(plate.getValue() + "");
        (v.findViewById(R.id.text_item)).setVisibility(View.INVISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            v.setBackground(context.getDrawable(R.drawable.grid_item_back));
        }
        return v;
    }
}
