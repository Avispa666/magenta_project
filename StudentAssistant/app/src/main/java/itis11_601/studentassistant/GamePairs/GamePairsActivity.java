package itis11_601.studentassistant.GamePairs;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;

import static itis11_601.studentassistant.GamePairs.GameLogic.counter;

/**
 * Created by 1 on 24.03.2017.
 */

public class GamePairsActivity extends DrawerActivity implements AdapterView.OnItemClickListener {

    private GridView gridView;
    private TextView score;
    private GameLogic gameLogic;
    private Thread t;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_pairs);
    }

    @Override
    protected void onFragmentCreated() {
        gameLogic = new GameLogic(Field.FIELD_COUNT);
        gridView = (GridView) findViewById(R.id.greed);
        IntGridAdapter adapter = new IntGridAdapter(this,
                gameLogic.getField().getAll());
        gridView.setAdapter(adapter);
        score = (TextView) findViewById(R.id.score);
        score.setText(gameLogic.getScore() + "");
        gridView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Plate[] plates = gameLogic.getField().getAll();
        if (!plates[position].isOpened()) {
            plates[position].setOpened(true);
            ColorFilter cf = new LightingColorFilter(Color.WHITE, Color.WHITE);
            view.getBackground().setColorFilter(cf);
            (view.findViewById(R.id.text_item)).setVisibility(View.VISIBLE);
            gameLogic.add(plates[position]);
            if (gameLogic.getPrev() == null)
                gameLogic.setPrev(view);
            counter++;
        }
        if (counter == 2) {
            if (!gameLogic.compareValues()) {
                gameLogic.closePlates();
                closeViews(gameLogic.getPrev(), view);
            } else {
                gameLogic.getPrev().getBackground().setColorFilter(new LightingColorFilter(
                        Color.GREEN, Color.GREEN));
                view.getBackground().setColorFilter(new LightingColorFilter(
                        Color.GREEN, Color.GREEN));
                GameLogic.platesLeft -= 2;
            }
            counter = 0;
            gameLogic.setPrev(null);
            gameLogic.countScore();
            score.setText(gameLogic.getScore() + "");
        }
        if (GameLogic.platesLeft == 0) {
            gameLogic.notifyListeners();
            alert();
        }
    }

    private void closeViews(final View v1, final View v2) {
        t = new Thread(new Runnable() {
            @Override
            public void run() {
                v1.getBackground().setColorFilter(null);
                v2.getBackground().setColorFilter(null);
                v1.findViewById(R.id.text_item).setVisibility(View.INVISIBLE);
                v2.findViewById(R.id.text_item).setVisibility(View.INVISIBLE);
            }
        });
        Handler h = new Handler();
        h.postDelayed(t, 300);
    }

    public void alert() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Plates Game")
                .setCancelable(false)
                .setMessage("Вы открыли все плитки за " + gameLogic.getScore() + " ходов")
                .setPositiveButton("Заново", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onFragmentCreated();
                    }
                })
                .setNegativeButton("Выйти", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GamePairsActivity.super.onBackPressed();
                    }
                }).create();
        dialog.show();
    }
}
