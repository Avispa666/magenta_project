package itis11_601.studentassistant.GamePairs;

import java.util.Random;

/**
 * Created by 1 on 24.03.2017.
 */

public class ValueGenerator {

    public static int[] generateNext(int n) {
        int[] tmp = new int[n];
        Random random = new Random();
        for (int i = 0; i < n / 2; i++) {
            tmp[i] = random.nextInt(n);
            tmp[n - i - 1] = tmp[i];
        }
        tmp = shake(tmp);
        return tmp;
    }

    private static int[] shake(int[] values) {
        Random r = new Random();
        int d = values.length;
        int[] result = new int[values.length];
        for (int i = 0; i < values.length; i++) {
            int c = r.nextInt(d);
            result[i] = values[c];
            for (int j = c; j < d - 1; j++) {
                values[j] = values[j + 1];
            }
            d--;
        }
        return result;
    }
}
