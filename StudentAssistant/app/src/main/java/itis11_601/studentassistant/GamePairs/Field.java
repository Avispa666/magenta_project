package itis11_601.studentassistant.GamePairs;

/**
 * Created by 1 on 24.03.2017.
 */

public class Field {

    private Plate[] plates;
    public static final int FIELD_COUNT = 20;

    public Field() {
        plates = null;
    }

    public void fillFieldByIntValues(int[] values) {
        plates = new Plate[values.length];
        for (int i = 0; i < plates.length; i++) {
            plates[i] = new Plate(values[i]);
        }
    }

    public Plate get(int n) {
        return plates[n];
    }

    public Plate[] getAll() {
        return plates;
    }
}
