package itis11_601.studentassistant.GamePairs;

/**
 * Created by 1 on 24.03.2017.
 */

public class FieldSingleton {

    private static Field field;

    public static Field getField() {
        if (field == null) {
            field = new Field();
        }
        return field;
    }

    public static void fieldClear() {
        field = null;
    }
}
