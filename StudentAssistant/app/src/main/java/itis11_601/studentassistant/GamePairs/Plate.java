package itis11_601.studentassistant.GamePairs;

/**
 * Created by 1 on 24.03.2017.
 */

public class Plate {

    private int value;
    private boolean opened;

    public Plate(int value) {
        this.value = value;
        opened = false;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public int getValue() {
        return value;
    }

    public boolean isOpened() {
        return opened;
    }

}
