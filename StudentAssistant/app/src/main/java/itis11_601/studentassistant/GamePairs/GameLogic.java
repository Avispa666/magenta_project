package itis11_601.studentassistant.GamePairs;

import android.view.View;

import java.util.ArrayList;

import itis11_601.studentassistant.Statistic.Observable;
import itis11_601.studentassistant.Statistic.OnStatisticListener;

/**
 * Created by 1 on 24.03.2017.
 */

public class GameLogic implements Observable {

    private Field field;
    public static int counter = 0;
    private ArrayList<Plate> pairs;
    public static int platesLeft;
    public View prev;
    private int score;
    private ArrayList<OnStatisticListener> listeners;
    private static final int MIN_COUNTERS = 10;

    public void start(int n) {
        listeners = new ArrayList<>();
        counter = 0;
        score = 0;
        FieldSingleton.fieldClear();
        field = FieldSingleton.getField();
        field.fillFieldByIntValues(
                ValueGenerator.generateNext(n));
        pairs = new ArrayList<>();
        platesLeft = field.getAll().length;
    }

    public Field getField() {
        return field;
    }

    public GameLogic(int n) {
        start(n);
    }

    public void add(Plate n) {
        pairs.add(n);
    }

    public boolean compareValues() {
        return pairs.get(0).getValue() == pairs.get(1).getValue();
    }

    public void closePlates() {;
        pairs.get(0).setOpened(false);
        pairs.get(1).setOpened(false);
        pairs.clear();
    }

    public void setPrev(View prev) {
        if (prev == null) {
            pairs.clear();
        }
        this.prev = prev;
    }

    public View getPrev() {
        return prev;
    }

    public int getScore() {
        return score;
    }

    public void countScore() {
        score++;
    }

    private int getResult() {
        int c = counter - MIN_COUNTERS;
        if (c < 7) {
            return 100 - c;
        }
        // TODO: 30.03.2017 create Result counter
        return c;
    }

    @Override
    public void register(OnStatisticListener o) {
        listeners.add(o);
    }

    @Override
    public void remove(OnStatisticListener o) {
        listeners.remove(o);
    }

    @Override
    public void notifyListeners() {
        for (OnStatisticListener listener : listeners) {
            listener.update(getResult());
        }
    }
}
