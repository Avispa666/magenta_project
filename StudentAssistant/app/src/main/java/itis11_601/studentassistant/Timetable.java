package itis11_601.studentassistant;

import android.content.Context;

import java.util.*;
public class Timetable {
    private ArrayList<Day> days;
    private boolean isEven;
    private final int DAYS_IN_WEEK = 7;
    private Context context;

    public ArrayList<Day> getDays() {
        return days;
    }

    public boolean isEven() {
        return isEven;
    }

    public Timetable(ArrayList<Day> days) {
        this.days = days;
        this.isEven = days.get(0).getDate().WEEK_OF_YEAR % 2 == 0;
    }

    public Timetable(Context context){
        this.context = context;
        fillDay();
        this.isEven = days.get(0).getDate().WEEK_OF_YEAR % 2 == 0;
    }

    private void calculateDay(boolean next){
        int move;
        if (next){
            move = 7;
        }
        else
            move = -7;
        for (Day day : days) {
            day.getDate().add(day.getDate().DAY_OF_MONTH, move);
        }
        this.isEven = !isEven();
    }

    public void fillDay(){
        days = new ArrayList<>();
        GregorianCalendar today = new GregorianCalendar();
        int toSunday = today.SUNDAY - today.DAY_OF_WEEK;
        today.add(today.DAY_OF_MONTH, toSunday);
        for (int i = 0; i < DAYS_IN_WEEK; i++) {
            today.add(today.DAY_OF_MONTH, 1);
            GregorianCalendar newDay = (GregorianCalendar) today.clone();
            days.add(new Day(context, newDay));
        }
    }

    public Timetable prev(){
        calculateDay(false);
        return this;
    }

    public Timetable next(){
        calculateDay(true);
        return this;
    }

}
