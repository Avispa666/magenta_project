package itis11_601.studentassistant.Subject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;

import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;

public class SubjectsActivity extends DrawerActivity {

    public static final String NAME = "Предметы";
    private FloatingActionButton fab;
    private ListView list;
    private TextView emptyText;
    private DBHelper dbHelper;
    private SubjectAdapter subjectAdapter;
    private ArrayList<Subject> subjects;
    private int pos;
    private final int REQUEST_CODE_ITEM = 3;
    private final int REQUEST_CODE_CREATE = 4;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subject_layout);
    }

    @Override
    protected void onFragmentCreated() {
        dbHelper = DBHelper.getDbHelper(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        list = (ListView) findViewById(R.id.list);
        emptyText = (TextView) findViewById(R.id.emptyText);
        subjects = dbHelper.getAllSubjects();
        Intent intent = getIntent();

        //Subject subject = intent.getParcelableExtra("Subject");
//        if (subject != null) {
//            if (subject.getTeacher() == null) subject.setTeacher("");
//            if (subject.getClassNum() == null) subject.setClassNum("");
//
//            if (subject.getName() != null) {
//                if (!intent.getBooleanExtra("createCheck", false)) {
//                    int position = intent.getIntExtra("position", 0);
//                    dbHelper.updateSubject(subjects.get(position), subject);
//                    subjects.set(position, subject);
//                } else {
//                    subjects.add(subjects.size(), subject);
//                    dbHelper.addSubject(subject);
//                }
//            }
//        }

        updatSubjects(subjects);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("SonyaBut", "Suuuukaaaaa");
                Intent i = new Intent(SubjectsActivity.this, SubjectOverviewActivity.class);
                i.putExtra("Subject", subjects.get(position));
                pos = position;
                startActivityForResult(i, REQUEST_CODE_ITEM);
            }
        });


        emptyListState();
        getSupportActionBar().setTitle(NAME);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubjectsActivity.this, SubjectCreateActivity.class);
                intent.putExtra("createCheck", true);
                startActivityForResult(intent, REQUEST_CODE_CREATE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.d("SonyaSubject", "I'm in result, huhuhu, and request code = " + requestCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_ITEM:
                    subjects.set(pos, (Subject) intent.getParcelableExtra("Subject"));
                    updatSubjects(subjects);
                    break;
                case REQUEST_CODE_CREATE:
                    subjects.add((Subject) intent.getParcelableExtra("Subject"));
                    updatSubjects(subjects);
                    break;
            }
        }
    }

    public void updatSubjects(ArrayList<Subject> subjects) {
        if (subjectAdapter != null) subjectAdapter = null;
        subjectAdapter = new SubjectAdapter(this, subjects);
        list.setAdapter(subjectAdapter);
        emptyListState();
    }

    private void emptyListState() {
        if (list.getCount() == 0) {
            emptyText.setVisibility(View.VISIBLE);
        } else {
            emptyText.setVisibility(View.INVISIBLE);
        }
    }

    public void subjectItem(View view) {
        Log.d("SonyaButton", "It work");
        final ImageButton ib = (ImageButton)view;
        PopupMenu popup = new PopupMenu(this, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.subject_menu, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        Log.d("SonyaButton", String.valueOf(ib.getId()));
                        dbHelper.removeSubject(subjects.get(ib.getId()));
                        subjects.remove(ib.getId());
                        updatSubjects(subjects);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }
}
