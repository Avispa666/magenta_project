package itis11_601.studentassistant.Subject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import itis11_601.studentassistant.ArchiveActivity;
import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.Note.NoteCreateActivity;
import itis11_601.studentassistant.Note.NoteSubjectAdapter;
import itis11_601.studentassistant.R;

public class SubjectOverviewActivity extends DrawerActivity implements View.OnClickListener {
    private ListView listOfNotes;
    private NoteSubjectAdapter adapter;
    private TextView name, teacher, classNum;
    private Button editButton, archive;
    private Subject subject;
    private ArrayList<Note> notes;
    private DBHelper dbHelper;
    private final int REQUEST_CODE_EDIT_SUBJECT = 3;
    private final int REQUEST_CODE_CREATE_NOTE = 4;
    private final int REQUEST_CODE_EDIT_NOTE = 5;
    private int pos;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subject_overview_layout);
    }

    @Override
    protected void onFragmentCreated() {
        final Intent intent = getIntent();
        subject = intent.getParcelableExtra("Subject");
       //  int pos = intent.getIntExtra("position", 0);
        name = (TextView) findViewById(R.id.name);
        teacher = (TextView) findViewById(R.id.teacher);
        classNum = (TextView) findViewById(R.id.classroom);
        editButton = (Button) findViewById(R.id.editButton);
        editButton.setText("Изменить");
        editButton.setOnClickListener(this);
        archive = (Button) findViewById(R.id.archive);
        archive.setOnClickListener(this);
        name.setText(subject.getName());
        teacher.setText(subject.getTeacher());
        classNum.setText(subject.getClassNum());
        dbHelper = DBHelper.getDbHelper(this);
        notes = dbHelper.getActualNote(subject, new GregorianCalendar());

        listOfNotes = (ListView) findViewById(R.id.listOfNotes);

        updatNotes(notes);
        Log.d("SonyaNote", "i end update");
        listOfNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SubjectOverviewActivity.this, NoteCreateActivity.class);
                Note curNote = notes.get(position);
                intent.putExtra("Subject", subject);
//                intent.putExtra("position", pos);
                intent.putExtra("Note", (Parcelable) curNote);
                pos = position;
//                intent.putExtra("notePosition", position);
//                Log.d("SonyaNote", "in begin i put " + position);
                startActivityForResult(intent, REQUEST_CODE_EDIT_NOTE);
            }
        });

    }

    public void updatNotes(ArrayList<Note> notes) {
        Log.d("SonyaNote", "i update note");
        if (adapter != null) adapter = null;
        adapter = new NoteSubjectAdapter(this, notes);
        listOfNotes.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.editButton:
                i = new Intent(SubjectOverviewActivity.this, SubjectCreateActivity.class);
                i.putExtra("Subject", subject);
//                i.putExtra("position", getIntent().getIntExtra("position", 0));
                startActivityForResult(i, REQUEST_CODE_EDIT_SUBJECT);
                break;
            case R.id.archive:
                i = new Intent(SubjectOverviewActivity.this, ArchiveActivity.class);
                i.putExtra("Subject", subject);
                startActivity(i);
                break;
            case R.id.createButton:
                i = new Intent(SubjectOverviewActivity.this, NoteCreateActivity.class);
                i.putExtra("Subject", subject);
                i.putExtra("createCheck", true);
                startActivityForResult(i, REQUEST_CODE_CREATE_NOTE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.d("SonyaNote", "i'm in result");
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_CREATE_NOTE:
                    notes.add((Note) intent.getParcelableExtra("Note"));
                    Log.d("SonyaSubject", String.valueOf(notes.get(0)));
                    updatNotes(notes);
                    break;
                case REQUEST_CODE_EDIT_SUBJECT:
                    subject = intent.getParcelableExtra("Subject");
                    name.setText(subject.getName());
                    teacher.setText(subject.getTeacher());
                    classNum.setText(subject.getClassNum());
                    break;
                case REQUEST_CODE_EDIT_NOTE:
                    Note note = intent.getParcelableExtra("Note");
                    notes.set(pos, (Note) note);
                    Log.d("SonyaNote", "I get from Note activity " + note.getTitle());
                    updatNotes(notes);
                    break;
            }
        }
        else Log.d("SonyaNote", "My result not OK");
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent();
        Log.d("SonyaSubject", "i put subject " + subject.getName());
        i.putExtra("Subject", subject);
        setResult(RESULT_OK, i);
        super.onBackPressed();
    }


    public void subjectItem(View view) {
        final ImageButton ib = (ImageButton)view;
        PopupMenu popup = new PopupMenu(this, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.subject_menu, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        Log.d("SonyaButton", String.valueOf(ib.getId()));
                        dbHelper.removeNote(subject, notes.get(ib.getId()));
                        notes.remove(ib.getId());
                        updatNotes(notes);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

}
