package itis11_601.studentassistant.Subject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.DrawerActivity;
import itis11_601.studentassistant.R;


/**
 * Created by Admin on 01.04.2017.
 */

public class SubjectCreateActivity extends DrawerActivity implements View.OnClickListener {

    private EditText name, teacher, classNum;
    private Button okButton;
    private Subject subject;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subject_edit_layout);
    }

    @Override
    protected void onFragmentCreated() {
        name = (EditText) findViewById(R.id.name);
        teacher = (EditText) findViewById(R.id.teacher);
        classNum = (EditText) findViewById(R.id.classroom);
        okButton = (Button) findViewById(R.id.OKButton);
        okButton.setOnClickListener(this);
        subject = getIntent().getParcelableExtra("Subject");
        if (subject != null) {
            name.setText(subject.getName());
            teacher.setText(subject.getTeacher());
            classNum.setText(subject.getClassNum());
        }
    }



    @Override
    public void onClick(View v) {
        Intent i = new Intent(SubjectCreateActivity.this, SubjectOverviewActivity.class);
        switch (v.getId()) {
            case R.id.OKButton:
                if (name.getText().toString().equals("")) {
                    Toast.makeText(SubjectCreateActivity.this, "Название не может быть пустым", Toast.LENGTH_SHORT).show();
                    break;
                }
                boolean create = getIntent().getBooleanExtra("createCheck", false);
                Subject newSubject = new Subject(name.getText().toString(), teacher.getText().toString(), classNum.getText().toString());
                DBHelper dbHelper = DBHelper.getDbHelper(this);
                if (create) dbHelper.addSubject(newSubject);
                else dbHelper.updateSubject(subject, newSubject);
                i.putExtra("Subject", newSubject);
                if (create)startActivityForResult(i, 3);
                else {
                    setResult(RESULT_OK, i);
                    finish();
                }
                Log.d("SonyaSubject", "I go in create activity");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == RESULT_OK) {
            Log.d("SonyaSubject", "i'm in result activity int create");
            Intent i = new Intent();
            i.putExtra("Subject", intent.getParcelableExtra("Subject"));
            setResult(RESULT_OK, i);
            finish();
        }
    }
}