package itis11_601.studentassistant.Subject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import itis11_601.studentassistant.R;

/**
 * Created by пользователь on 07.04.2017.
 */

public class SubjectAdapter extends BaseAdapter {
    ArrayList<Subject> subjects;
    SubjectsActivity activity;
    LayoutInflater inflater;

    public SubjectAdapter(SubjectsActivity context, ArrayList<Subject> subjects) {
        this.activity = context;
        this.subjects = subjects;
        inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public Object getItem(int position) {
        return subjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Subject subject = (Subject) getItem(position);
        View view = inflater.inflate(R.layout.subject_item, parent, false);
        ((TextView) view.findViewById(R.id.subject)).setText(subject.getName());
        ((ImageButton) view.findViewById(R.id.imageButton)).setFocusable(false);
        ((ImageButton) view.findViewById(R.id.imageButton)).setId(position);

        return view;
    }
}

