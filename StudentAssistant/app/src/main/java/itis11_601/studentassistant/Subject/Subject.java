package itis11_601.studentassistant.Subject;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import itis11_601.studentassistant.DBHelper;
import itis11_601.studentassistant.Note.Note;
import itis11_601.studentassistant.Note.NoteHandler;

public class Subject extends NoteHandler implements Parcelable{
    private String name;
    private String teacher;
    private String classNum;
    private Context context;


    public Subject(ArrayList<Note> notes, String name, String teacher, String classNum) {
        super(notes);
        if (name == null) name = "";
        if (teacher == null) teacher = "";
        if (classNum == null) classNum = "";
        this.name = name;
        this.teacher = teacher;
        this.classNum = classNum;
    }

    public Subject(String name, String teacher, String classNum) {
        this(null, name, teacher, classNum);
    }

    public void addNote(Note note) {
        if (getNotes() == null) setNotes(new ArrayList<Note>());
        getNotes().add(note);

        DBHelper dbHelper = DBHelper.getDbHelper(context);
        dbHelper.addNote(this, note);
    }

    public void removeNote(Note note) {
        getNotes().remove(note);

        DBHelper dbHelper = DBHelper.getDbHelper(context);
        dbHelper.removeNote(this, note);
    }

    public void updateNote(Note lastNote, Note newNote) {
        if (getNotes() != null) {
            int index = getNotes().indexOf(lastNote);
            getNotes().set(index, newNote);
        }

        DBHelper dbHelper = DBHelper.getDbHelper(context);
        dbHelper.updateNote(this, lastNote, newNote);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(teacher);
        dest.writeString(classNum);
    }

    public static final Parcelable.Creator<Subject> CREATOR = new Parcelable.Creator<Subject>() {

        @Override
        public Subject createFromParcel(Parcel source) {
            return new Subject(source);
        }

        @Override
        public Subject[] newArray(int size) {
            return new Subject[0];
        }
    };

    private Subject(Parcel parcel) {
        this.name = parcel.readString();
        this.teacher = parcel.readString();
        this.classNum = parcel.readString();
    }

    @Override
    public String toString() {
        return getName();
    }
}
