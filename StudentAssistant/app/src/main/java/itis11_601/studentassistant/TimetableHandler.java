package itis11_601.studentassistant;
public class TimetableHandler {
    private Timetable current;

    public Timetable getCurrent() {
        return current;
    }

    public TimetableHandler(Timetable current) {
        this.current = current;
    }

    public TimetableHandler() {
    }

    public void next(){
        current = current.next();
    }

    public void prev(){
        current = current.prev();
    }
}
